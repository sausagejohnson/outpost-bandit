Outpost Bandit
==============

Outpost Bandit is a short casual game available for free for Windows, MacOS X and Linux. You are free to spread it around to your friends. However:

1) You may not offer the game on a download site that contains ad-ware.
2) You may not offer the game on a site that contains immoral, political, or social justice material.
3) You must not change the original file structure: do not remove, change or add files.
4) You may not sell this game, give it away for free as part of a paid compilation, or pay any money for it.


Rules
-----

Pilot your Bandit craft through the asteroids looking for the Blue Ore, the property of the aliens. The Ore is held in Outpost Satellites.

Use your HUD to locate the Satellite that contains the next collectible Ore. Return each Ore to your central teleporter device. Once a piece of Ore is teleported away, the next available Ore will be shown on your HUD.

Once four pieces of Ore are collected, you progress to the next level. There are ten levels to complete.

The HUD will also show the position of the Aliens that are teleporting into your region.

There are two types of Aliens, the standard ship, and the Seeker. 

As you progress through the levels, the density of the asteroids will increase and so will the chances of encountering Seekers.

You do not have a weapon. You are only equipped with a movable shield. The shield can only defend a small part of the Bandit craft at a time. And there is only limited time to use the shield before a recharge is required.

If the Aliens manage to shoot or collide with the Bandit, a piece of the Bandit will be destroyed. Once the Aliens manage to break their way into the Bandit's core, the craft will be destroyed and the game will end.

Collisions with asteroids have a concussive effect. A piece of the Bandit will be lost and the craft will become unresponsive for two seconds.

If you try to leave the play zone, your ship will slow to a crawl, and your HUD will flash as a warning. You must re-enter the play zone. The Aliens can easily pick you off outside the play zone.

There is a Bonus for taking risks to complete a Level quickly. The Bonus will start depleting after 30 seconds.

Controls
--------

The player can use either Keyboard or a Game Controller. 

The game supports many common game controllers, XBox, Playstation 4, and many exotic rare types too. For any that don't work, or only partly work, press F1 to remap the controls. When connecting a controller, an icon will be displayed in the top right hand corner.

Unless the keyboard controls have been remapped, the standard keys are:

WASD:		Shield Position
Cursors:	Bandit Movement
Space:		Start Game
Escape:		Exit re-map screen, go back to the title screen, or Quit the game
F1:			Re-map screen for Controllers and Keys
Zero:		Take a screenshot
Enter:		Select control to re-map (in the re-map screen)
Alt Enter:	Toggle Fullscreen Mode


Contact
-------
You are welcome to get in touch with me at sausage@waynejohnson.net or http://alienabductionunit.com


About the Development of the game
---------------------------------
Outpost Bandit was written in c++ using the Orx 2D Game Engine (orx-project.org). 

All coding, graphics, music and sound have been done by Wayne Johnson of the Alien Abduction Unit (alienabductionunit.com).

The purpose of creating the game was to promote the use of the Orx 2D engine, many tutorials having been produced as a result of its development. Some of these tutorials are:

* Analog Joystick / Gamepad Control     - http://orx-project.org/wiki/en/tutorials/analog_joystick_gamepad_control
* How to Override a Controller Mapping  - http://orx-project.org/wiki/en/tutorials/overriding_controller_mapping
* Joint Welding							- http://orx-project.org/wiki/en/tutorials/weld_joint_on_objects_with_bodies
* Creating a Heads Up Display (HUD)		- http://orx-project.org/wiki/en/tutorials/viewport_heads_up_display
* Object with many Dynamic Body Parts   - http://orx-project.org/wiki/en/tutorials/creating_an_object_with_many_parts
* Futurama Appearance Effect (FX)		- http://orx-project.org/wiki/en/tutorials/spawners/futurama_appearance
* Switch Music with Sound Buses         - http://orx-project.org/wiki/en/tutorials/sound_buses
* Rendering a ViewPort to a Texture     - http://orx-project.org/wiki/en/tutorials/viewport_render_to_texture
* Retrieving and Changing Config Values - http://orx-project.org/wiki/en/tutorials/retrieving_changing_config_values
* Application Icons						- http://orx-project.org/wiki/en/tutorials/display/application_icons
* Toggling Fullscreen Mode			    - http://orx-project.org/wiki/en/examples/input/combination_keys_with_combinelist
* Fixing Cameras to Objects 			- http://orx-project.org/wiki/en/examples/cameras
* Using the Orx Profiler 				- http://orx-project.org/wiki/en/tutorials/tools/profiler
* Passing items between objects			- http://orx-project.org/wiki/en/tutorials/passing_objects
