##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Strip_Release
ProjectName            :=Outpost
ConfigurationName      :=Strip_Release
WorkspacePath          := "C:\Work\Dev\orx-projects\outpost-bandit\build\windows\codelite"
ProjectPath            := "C:\Work\Dev\orx-projects\outpost-bandit\build\windows\codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=05/03/2019
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/MinGW-6.3.0/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-6.3.0/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/Outpost.exe
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Outpost.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-6.3.0/bin/windres.exe
LinkOptions            :=  -s -mwindows -static -static-libgcc -static-libstdc++
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)$(ORX)/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx 
ArLibs                 :=  "orx" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-6.3.0/bin/ar.exe rcu
CXX      := C:/MinGW-6.3.0/bin/g++.exe
CC       := C:/MinGW-6.3.0/bin/gcc.exe
CXXFLAGS :=  -g -O2 -ffast-math -msse2 -fno-exceptions -fno-rtti  $(Preprocessors)
CFLAGS   :=  -g -O2 -ffast-math -msse2 -fno-exceptions -fno-rtti $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-6.3.0/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
Objects0=$(IntermediateDirectory)/src_Outpost.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Alien.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Asteroid.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_ControlMenu.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_DistanceEngine.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_resource.rc$(ObjectSuffix) $(IntermediateDirectory)/src_Seeker.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Shield.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Ship.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_SplashScreen.cpp$(ObjectSuffix) \
	



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c if exist $(ORX)\lib\dynamic\orx.dll copy /Y $(ORX)\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_Outpost.cpp$(ObjectSuffix): ../../../src/Outpost.cpp $(IntermediateDirectory)/src_Outpost.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Outpost.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Outpost.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Outpost.cpp$(DependSuffix): ../../../src/Outpost.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Outpost.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Outpost.cpp$(DependSuffix) -MM "../../../src/Outpost.cpp"

$(IntermediateDirectory)/src_Outpost.cpp$(PreprocessSuffix): ../../../src/Outpost.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Outpost.cpp$(PreprocessSuffix) "../../../src/Outpost.cpp"

$(IntermediateDirectory)/src_Alien.cpp$(ObjectSuffix): ../../../src/Alien.cpp $(IntermediateDirectory)/src_Alien.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Alien.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Alien.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Alien.cpp$(DependSuffix): ../../../src/Alien.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Alien.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Alien.cpp$(DependSuffix) -MM "../../../src/Alien.cpp"

$(IntermediateDirectory)/src_Alien.cpp$(PreprocessSuffix): ../../../src/Alien.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Alien.cpp$(PreprocessSuffix) "../../../src/Alien.cpp"

$(IntermediateDirectory)/src_Asteroid.cpp$(ObjectSuffix): ../../../src/Asteroid.cpp $(IntermediateDirectory)/src_Asteroid.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Asteroid.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Asteroid.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Asteroid.cpp$(DependSuffix): ../../../src/Asteroid.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Asteroid.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Asteroid.cpp$(DependSuffix) -MM "../../../src/Asteroid.cpp"

$(IntermediateDirectory)/src_Asteroid.cpp$(PreprocessSuffix): ../../../src/Asteroid.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Asteroid.cpp$(PreprocessSuffix) "../../../src/Asteroid.cpp"

$(IntermediateDirectory)/src_ControlMenu.cpp$(ObjectSuffix): ../../../src/ControlMenu.cpp $(IntermediateDirectory)/src_ControlMenu.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/ControlMenu.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ControlMenu.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ControlMenu.cpp$(DependSuffix): ../../../src/ControlMenu.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ControlMenu.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ControlMenu.cpp$(DependSuffix) -MM "../../../src/ControlMenu.cpp"

$(IntermediateDirectory)/src_ControlMenu.cpp$(PreprocessSuffix): ../../../src/ControlMenu.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ControlMenu.cpp$(PreprocessSuffix) "../../../src/ControlMenu.cpp"

$(IntermediateDirectory)/src_DistanceEngine.cpp$(ObjectSuffix): ../../../src/DistanceEngine.cpp $(IntermediateDirectory)/src_DistanceEngine.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/DistanceEngine.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_DistanceEngine.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_DistanceEngine.cpp$(DependSuffix): ../../../src/DistanceEngine.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_DistanceEngine.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_DistanceEngine.cpp$(DependSuffix) -MM "../../../src/DistanceEngine.cpp"

$(IntermediateDirectory)/src_DistanceEngine.cpp$(PreprocessSuffix): ../../../src/DistanceEngine.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_DistanceEngine.cpp$(PreprocessSuffix) "../../../src/DistanceEngine.cpp"

$(IntermediateDirectory)/src_resource.rc$(ObjectSuffix): ../../../src/resource.rc
	$(RcCompilerName) -i "C:/Work/Dev/orx-projects/outpost-bandit/src/resource.rc" $(RcCmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/src_resource.rc$(ObjectSuffix) $(RcIncludePath)
$(IntermediateDirectory)/src_Seeker.cpp$(ObjectSuffix): ../../../src/Seeker.cpp $(IntermediateDirectory)/src_Seeker.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Seeker.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Seeker.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Seeker.cpp$(DependSuffix): ../../../src/Seeker.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Seeker.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Seeker.cpp$(DependSuffix) -MM "../../../src/Seeker.cpp"

$(IntermediateDirectory)/src_Seeker.cpp$(PreprocessSuffix): ../../../src/Seeker.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Seeker.cpp$(PreprocessSuffix) "../../../src/Seeker.cpp"

$(IntermediateDirectory)/src_Shield.cpp$(ObjectSuffix): ../../../src/Shield.cpp $(IntermediateDirectory)/src_Shield.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Shield.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Shield.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Shield.cpp$(DependSuffix): ../../../src/Shield.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Shield.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Shield.cpp$(DependSuffix) -MM "../../../src/Shield.cpp"

$(IntermediateDirectory)/src_Shield.cpp$(PreprocessSuffix): ../../../src/Shield.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Shield.cpp$(PreprocessSuffix) "../../../src/Shield.cpp"

$(IntermediateDirectory)/src_Ship.cpp$(ObjectSuffix): ../../../src/Ship.cpp $(IntermediateDirectory)/src_Ship.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/Ship.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Ship.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Ship.cpp$(DependSuffix): ../../../src/Ship.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Ship.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Ship.cpp$(DependSuffix) -MM "../../../src/Ship.cpp"

$(IntermediateDirectory)/src_Ship.cpp$(PreprocessSuffix): ../../../src/Ship.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Ship.cpp$(PreprocessSuffix) "../../../src/Ship.cpp"

$(IntermediateDirectory)/src_SplashScreen.cpp$(ObjectSuffix): ../../../src/SplashScreen.cpp $(IntermediateDirectory)/src_SplashScreen.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/outpost-bandit/src/SplashScreen.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_SplashScreen.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_SplashScreen.cpp$(DependSuffix): ../../../src/SplashScreen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_SplashScreen.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_SplashScreen.cpp$(DependSuffix) -MM "../../../src/SplashScreen.cpp"

$(IntermediateDirectory)/src_SplashScreen.cpp$(PreprocessSuffix): ../../../src/SplashScreen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_SplashScreen.cpp$(PreprocessSuffix) "../../../src/SplashScreen.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


