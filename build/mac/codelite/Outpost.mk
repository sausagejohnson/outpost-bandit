##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release_x64
ProjectName            :=Outpost
ConfigurationName      :=Release_x64
WorkspacePath          :=/Users/canberra-air/Documents/outpost-bandit/build/mac/codelite
ProjectPath            :=/Users/canberra-air/Documents/outpost-bandit/build/mac/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=canberra-air
Date                   :=05/03/2019
CodeLitePath           :="/Users/canberra-air/Library/Application Support/CodeLite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -dynamiclib -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/Outpost
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="Outpost.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -s -m64 -L/usr/lib64 -mmacosx-version-min=10.7 -dead_strip -framework Foundation -framework AppKit
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../../../../canberra-air/Documents/orx/code/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx 
ArLibs                 :=  "orx" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../../../../canberra-air/Documents/orx/code/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -ffast-math -g -msse2 -O2 -m64 -fno-exceptions -fno-rtti -mmacosx-version-min=10.7 -gdwarf-2 -Wno-write-strings -std=c++11 $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -O2 -m64 -fno-exceptions -fno-rtti -mmacosx-version-min=10.7 -gdwarf-2 -Wno-write-strings -std=c++11 $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/Applications/codelite.app/Contents/SharedSupport/
Objects0=$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Alien.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cp -f /Users/canberra-air/Documents/orx/code/lib/dynamic/liborx*.dylib ../../../bin
	@echo Done

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix): ../../../src/Ship.cpp $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Ship.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix): ../../../src/Ship.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix) -MM ../../../src/Ship.cpp

$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix): ../../../src/Ship.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix) ../../../src/Ship.cpp

$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix): ../../../src/Shield.cpp $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Shield.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix): ../../../src/Shield.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix) -MM ../../../src/Shield.cpp

$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix): ../../../src/Shield.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix) ../../../src/Shield.cpp

$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(ObjectSuffix): ../../../src/Seeker.cpp $(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Seeker.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(DependSuffix): ../../../src/Seeker.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(DependSuffix) -MM ../../../src/Seeker.cpp

$(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(PreprocessSuffix): ../../../src/Seeker.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Seeker.cpp$(PreprocessSuffix) ../../../src/Seeker.cpp

$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(ObjectSuffix): ../../../src/DistanceEngine.cpp $(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/DistanceEngine.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(DependSuffix): ../../../src/DistanceEngine.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(DependSuffix) -MM ../../../src/DistanceEngine.cpp

$(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(PreprocessSuffix): ../../../src/DistanceEngine.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_DistanceEngine.cpp$(PreprocessSuffix) ../../../src/DistanceEngine.cpp

$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(ObjectSuffix): ../../../src/ControlMenu.cpp $(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/ControlMenu.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(DependSuffix): ../../../src/ControlMenu.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(DependSuffix) -MM ../../../src/ControlMenu.cpp

$(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(PreprocessSuffix): ../../../src/ControlMenu.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_ControlMenu.cpp$(PreprocessSuffix) ../../../src/ControlMenu.cpp

$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(ObjectSuffix): ../../../src/Asteroid.cpp $(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Asteroid.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(DependSuffix): ../../../src/Asteroid.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(DependSuffix) -MM ../../../src/Asteroid.cpp

$(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(PreprocessSuffix): ../../../src/Asteroid.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Asteroid.cpp$(PreprocessSuffix) ../../../src/Asteroid.cpp

$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(ObjectSuffix): ../../../src/Alien.cpp $(IntermediateDirectory)/up_up_up_src_Alien.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Alien.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(DependSuffix): ../../../src/Alien.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(DependSuffix) -MM ../../../src/Alien.cpp

$(IntermediateDirectory)/up_up_up_src_Alien.cpp$(PreprocessSuffix): ../../../src/Alien.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Alien.cpp$(PreprocessSuffix) ../../../src/Alien.cpp

$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(ObjectSuffix): ../../../src/Outpost.cpp $(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/Outpost.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(DependSuffix): ../../../src/Outpost.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(DependSuffix) -MM ../../../src/Outpost.cpp

$(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(PreprocessSuffix): ../../../src/Outpost.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Outpost.cpp$(PreprocessSuffix) ../../../src/Outpost.cpp

$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(ObjectSuffix): ../../../src/SplashScreen.cpp $(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/outpost-bandit/src/SplashScreen.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(DependSuffix): ../../../src/SplashScreen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(DependSuffix) -MM ../../../src/SplashScreen.cpp

$(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(PreprocessSuffix): ../../../src/SplashScreen.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_SplashScreen.cpp$(PreprocessSuffix) ../../../src/SplashScreen.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


