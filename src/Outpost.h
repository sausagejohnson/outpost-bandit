#ifndef __MYGAME_H_
#define __MYGAME_H_

//! Includes
// The following define skips compilation of ScrollEd (map editor) for now
#define __NO_SCROLLED__
#include "Scroll.h"
#include "DistanceEngine.h"

#define PLAYING 0
#define REMAPPING 1
#define SPLASH_SCREEN 2

//! MyGame class
class Outpost : public Scroll<Outpost>
{
public:
	orxVECTOR GetLastKnownOutpostPosition();
	orxVECTOR GetCurrentOutpostPosition();
	void RemoveAllOreScrollObjects();
	void CreateOreAtRandomSatellite();
	void AddToScore(int points);
	//void SetSpeedIndicator(orxFLOAT distance);
	void LoseControl();
	orxBOOL GetControlsEnabled();
	void DisableControls();
	void AddOreCollected(int count);
	int GetOreCollectedCount();
	void ProgressToNextLevel();
	void HideGameScene();
	void SetGameOver();
	void SetStartGame();
	orxBOOL IsGameEnabled();
	void ReportAlienDistance(orxFLOAT distance);
	int GetLevelFromConfig();
	void SetAlienSpawnRatioToConfig();
	void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig);
	void ShakeCamera();
	void MoveCameraScrollStars();
	void ReduceBonus();
	void StartBonusCountdown();
	void StopBonusCountdown();
	void ResetBonus();
	void StartLevelCountdown();
	void StopLevelCountdown();
	void InitGame();
	
private:
	void Update(const orxCLOCK_INFO &_rstInfo);
	void CameraUpdate(const orxCLOCK_INFO &_rstInfo);
	const orxFLOAT ROTATION_MAX_DISTANCE = 0.15;
	const orxFLOAT MAX_FORCE = 2500; //40;
	const orxFLOAT ROTATION_UNIT = 0.0020;

	float extraRotationToAdd = 0; //used by the shield position. Gets picked up by the Update.
	float defaultGameWidth = 0;
	float defaultGameHeight = 0;

	orxFLOAT controlLossTimeout = 0;
	orxFLOAT currentZoom = 1.0;
	DistanceEngine *distanceEngine = new DistanceEngine();
	int inputMode = SPLASH_SCREEN;
	orxFLOAT joystickThreshold = 0;

	//! Initialize the program
	virtual orxSTATUS Init();
	//! Callback called every frame
	virtual orxSTATUS Run();
	//! Exit the program
	virtual void      Exit();
	virtual void BindObjects();
	virtual orxSTATUS Bootstrap() const;
	virtual void DoInputs(const orxCLOCK_INFO &_pstClockInfo);
	virtual void DoRemapMenuInputs();
	void ApplyShieldPosAndRotToOutpost(float extraRotation);
	//orxSTATUS orxFASTCALL EventHandler(const orxEVENT *_pstEvent);
	void CreateAsteroids();
	ScrollObject* CreateScrollObjectToScene(const orxSTRING name);
	orxOBJECT* CreateOrxObjectToScene(const orxSTRING name);
	void ApplyFadeInToObject(orxOBJECT *object);
	void CreateGameObjects();
	void ShowGameScene();
	void CreateTitle();
	void RemoveTitle();
	void CreateControllerIcon();
	void DeleteControllerIcon();
	void SetLevelInConfig(int newLevel);
	void UpdateGameLevelDetailsInConfig();
	void DeleteAgedAsteroids();
	void CreateLevelBanner();
	orxOBJECT* GetChildObjectFromScene(orxSTRING name);
	orxOBJECT* GetObjectByName(orxSTRING nameOfObjectToFind);
	orxOBJECT* FindObjectByName(orxSTRING nameOfObjectToFind);
	void DeleteCurrentAsteroidCreator();
	void RecreateAsteroidCreator();
	void DeleteCurrentAlienLauncher();
	void RecreateAlienLauncher();
	void SetZoomBasedOnDistance(const orxCLOCK_INFO &_pstClockInfo);
	void ResetZoom();
	void SwitchToTitleMusic();
	void SwitchToGameMusic();
	void ReduceShield(const orxCLOCK_INFO &_pstClockInfo);
	void IncreaseShield(const orxCLOCK_INFO &_pstClockInfo);
	void ApplyPositiveRotation(const orxCLOCK_INFO &_pstClockInfo);
	void ApplyNegativeRotation(const orxCLOCK_INFO &_pstClockInfo);
	orxU32 GetU32FromConfig(orxSTRING section, orxSTRING property);
	void LoadHighScoreFromConfig();
	void SaveHighScoreToDisk();
	void UpdateHighScore(); //sets highscore if score is greater than last
	void SetHighScoreText();
	void UpdateKeyInstructionsText();
	void UpdateBonusValueDisplay();
	void UpdateScoreDisplay();

    void OutputTestInfoToLog();

	orxBOOL quit = orxFALSE;
};

#endif // __MYGAME_H_
