
#include "Outpost.h"

class Seeker : public ScrollObject
{
public:

private:
	orxFLOAT movementSpeed;
	orxU16 AlienSelfFlag = 8;
	orxU16 AsteroidCheckMask = 32;
	int currentLevel;

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider,
		const orxSTRING _zPartName,
		const orxSTRING _zColliderPartName,
		const orxVECTOR &_rvPosition,
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);

};
