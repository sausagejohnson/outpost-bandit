#include "DistanceEngine.h"

DistanceEngine::DistanceEngine() {
	for (int x = 0; x < count; x++) {
		distances[x] = 0;
	}
}

float DistanceEngine::GetShortestDistance() {
	float shortest = distances[0];

	for (int x = 1; x < count; x++) {
		if (distances[x] < shortest && distances[x] != 0) {
			shortest = distances[x];
		}
	}

	return shortest;
}

void DistanceEngine::ReportDistance(float distance) {
	distances[currentIndex] = distance;

	if (currentIndex >= count) {
		currentIndex = 0;
	}
}