#include "Ship.h"

void Ship::OnCreate()
{
	orxVECTOR speed = orxVECTOR_0;
	this->GetSpeed(speed, orxFALSE);


}

void Ship::OnDelete()
{
	// Do nothing when deleted
}

//void Ship::CreateCameraShake() {
	//cameraHandle = orxObject_CreateFromConfig("CameraHandle");

	//orxOBJECT *ship = this->GetOrxObject();
	//orxObject_SetParent(cameraHandle, ship);
	//orxObject_SetOwner(cameraHandle, ship);
//}


void Ship::Update(const orxCLOCK_INFO &_rstInfo)
{
	

	// Always initialize thy variables
	/*orxVECTOR speed = orxVECTOR_0;
	orxVECTOR position = orxVECTOR_0;
	GetPosition(position, orxTRUE);

	if (orxInput_IsActive("MoveLeft") && position.fX > -350) {
		speed.fX = -movementSpeed;
	}

	if (orxInput_IsActive("MoveRight") && position.fX < 350) {
		speed.fX = movementSpeed;
	}

	for (orxOBJECT *pstChild = orxObject_GetOwnedChild(GetOrxObject());
		pstChild;
		pstChild = orxObject_GetOwnedSibling(pstChild))
	{
		const orxSTRING name = orxObject_GetName(pstChild);
		if (orxString_Compare(name, "ShipSpawnerSwitch") == 0) {
			orxObject_Enable(pstChild, orxInput_IsActive("Fire"));
		}
	}

	SetSpeed(speed);*/

}

void Ship::UpdateMass() {
	//orxOBJECT *ship = this->GetOrxObject();
	//orxBODY *body = orxOBJECT_GET_STRUCTURE(ship, BODY);

	//for (orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL); part; part = orxBody_GetNextPart(body, part)) {
	//	if (orxString_Compare(orxBody_GetPartName(part), "MassPart") == 0) {
	//		int count = ChildCount();
	//		int inverse = MAX_CHILD_COUNT;
	//		orxFLOAT size = (orxFLOAT)inverse * 0.25;
	//		orxBody
	//	}
	//}
}

int Ship::ChildCount() {
	int count = 0;

	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		count++;
	}
	return count;
}

void Ship::DeleteChildByName(const orxSTRING childName) {

	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			child->SetLifeTime(0);
			return;
		}
	}
}

orxVECTOR Ship::GetChildPositionByName(const orxSTRING childName) {
	orxVECTOR position = orxVECTOR_0;

	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			child->GetPosition(position, orxTRUE);
			return position;
		}
	}

	return position;
}


ScrollObject* Ship::GetChildByName(const orxSTRING childName) {

	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		//orxLOG("Child here: %s", child->GetModelName());
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			return child;
		}
	}

	return orxNULL;
}

void Ship::DeleteBodyPartByName(const orxSTRING partName) {
	orxOBJECT *ship = this->GetOrxObject();
	orxBODY *body = orxOBJECT_GET_STRUCTURE(ship, BODY);

	for (orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL); part; part = orxBody_GetNextPart(body, part)) {
		if (orxString_Compare(orxBody_GetPartName(part), partName) == 0) {
			orxBody_RemovePart(part);
		}
	}
}

/* Strip "Part" out of the name. eg BottomLeftPart4 to BottomLeft4 */
orxSTRING Ship::PartToObjectName(const orxSTRING orxPartName) {
	const orxCHAR *pointerToPart = orxString_SearchString(orxPartName, "Part");
	int indexToPart = pointerToPart - orxPartName;
	int indexToNumber = indexToPart + 4;
	int length = orxString_GetLength(orxPartName);

	static orxCHAR newString[255] = {};
	for (int x = 0; x < length; x++) {
		newString[x] = orxCHAR_NULL;
	}
	orxCHAR *secondSectionOfString = newString + indexToPart;
	orxString_NCopy(newString, orxPartName, indexToPart);
	if (indexToNumber < length) {
		orxString_NCopy(secondSectionOfString, &orxPartName[length] - 1, 1);
	}
	return newString;
}


orxBOOL Ship::OnCollide(ScrollObject *_poCollider, 
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_Compare(colliderName, "AlienBullet") == 0 ||
		orxString_Compare(colliderName, "Alien") == 0 ||
		orxString_Compare(colliderName, "Seeker") == 0
		) {
		orxVECTOR bulletPosition = orxVECTOR_0;

		_poCollider->GetPosition(bulletPosition, orxTRUE);
		_poCollider->SetLifeTime(0);

		orxOBJECT *bulletSplatter = orxObject_CreateFromConfig("BulletSplatter");
		orxObject_SetPosition(bulletSplatter, &bulletPosition);

		if (
			orxString_SearchString(_zPartName, "Top") != orxNULL ||
			orxString_SearchString(_zPartName, "Right") != orxNULL ||
			orxString_SearchString(_zPartName, "Bottom") != orxNULL ||
			orxString_SearchString(_zPartName, "Left") != orxNULL
			) {

			//translate the name of the part into the target child object name
			//eg BottomLeftPart4 to BottomLeft4
			orxSTRING childObjectName = this->PartToObjectName(_zPartName);

			orxVECTOR partPosition = orxVECTOR_0;
			partPosition = GetChildPositionByName(childObjectName);

			orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");
			orxObject_SetPosition(explosion, &partPosition);

			this->DeleteChildByName(childObjectName);
			this->DeleteBodyPartByName(_zPartName);
		}

	}

	if (orxString_Compare(colliderName, "Asteroid") == 0) {
		if (
			orxString_SearchString(_zPartName, "Top") != orxNULL ||
			orxString_SearchString(_zPartName, "Right") != orxNULL ||
			orxString_SearchString(_zPartName, "Bottom") != orxNULL ||
			orxString_SearchString(_zPartName, "Left") != orxNULL
			) {

			//translate the name of the part into the target child object name
			//eg BottomLeftPart4 to BottomLeft4
			orxSTRING childObjectName = this->PartToObjectName(_zPartName);

			orxVECTOR partPosition = orxVECTOR_0;
			partPosition = GetChildPositionByName(childObjectName);

			orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");
			orxObject_SetPosition(explosion, &partPosition);

			//CreateCameraShake();
			Outpost::GetInstance().ShakeCamera();

			this->DeleteChildByName(childObjectName);
			this->DeleteBodyPartByName(_zPartName);

			orxVECTOR speed = orxVECTOR_0;
			this->GetSpeed(speed, orxFALSE);
			if (speed.fX < 5 && speed.fY < 5) {
				speed = orxVECTOR_0;
			}
			if (speed.fX == 0 && speed.fY == 0) {
				orxVECTOR asteroidPos = orxVECTOR_0;
				_poCollider->GetPosition(asteroidPos, orxTRUE);
				orxVector_Add(&speed, &partPosition, &asteroidPos);
				orxVector_2DRotate(&speed, &speed, (orxMATH_KF_DEG_TO_RAD * 180));
				orxVector_Normalize(&speed, &speed);
				orxVector_Mulf(&speed, &speed, 500);
			}
			else {
				orxVector_2DRotate(&speed, &speed, (orxMATH_KF_DEG_TO_RAD * 180));
				orxVector_Normalize(&speed, &speed);
				orxVector_Mulf(&speed, &speed, 500);
			}
			this->SetSpeed(speed, orxFALSE);

			Outpost::GetInstance().LoseControl();
		}
		

	}

	if (orxString_SearchString(colliderName, "Satellite") != orxNULL ) {
		orxOBJECT *outpostObject = this->GetOrxObject();

		orxOBJECT *satellite = _poCollider->GetOrxObject();
		orxOBJECT *oreObject = orxNULL; //= orxOBJECT(orxObject_GetChild(satellite));

		for (orxOBJECT *child = orxOBJECT(orxObject_GetChild(satellite)); child; child = orxOBJECT(orxObject_GetSibling(child)) ) {
			//orxLOG("childzzzz %s ", orxObject_GetName(child));
			if (orxString_Compare(orxObject_GetName(child), "Ore") == 0) {
				oreObject = child;
			}
		}

		if (oreObject == orxNULL) {
			return true;
		}

		orxVECTOR outpostPos = orxVECTOR_0;
		this->GetPosition(outpostPos, orxTRUE);

		orxObject_Detach(oreObject);
		orxObject_SetPosition(oreObject, &outpostPos);
		orxObject_Attach(oreObject, outpostObject);
		orxObject_SetParent(oreObject, outpostObject);
		orxObject_SetOwner(oreObject, outpostObject);

		Outpost::GetInstance().PlaySoundOn(oreObject, "EventSound");

		Outpost::GetInstance().AddToScore(150);

	}

	if (orxString_Compare(colliderName, "DropOffInner") == 0) {
		//orxOBJECT *outpostObject = this->GetOrxObject();
		ScrollObject *ore = GetChildByName("Ore");
		if (ore == orxNULL) {
			return true;
		}

		orxOBJECT *oreObject = ore->GetOrxObject();

		//orxVECTOR outpostPos = orxVECTOR_0;
		//this->GetPosition(outpostPos, orxTRUE);

		orxVECTOR dropOffPosition = orxVECTOR_0;
		ScrollObject *dropOff = _poCollider;
		dropOff->GetPosition(dropOffPosition, orxTRUE);
		orxOBJECT *dropOffObject = dropOff->GetOrxObject();

		orxObject_Detach(oreObject);
		orxObject_SetParent(oreObject, dropOffObject);
		orxObject_SetWorldPosition(oreObject, &dropOffPosition);
		//orxObject_Attach(oreObject, dropOffObject);
		orxObject_SetOwner(oreObject, orxNULL);

		orxObject_AddTimeLineTrack(oreObject, "OreTeleportTrack");
		Outpost::GetInstance().PlaySoundOn(oreObject, "EventSound");

		//orxOBJECT *newOre = orxObject_CreateFromConfig("Ore");
		Outpost::GetInstance().AddOreCollected(1);
		if (Outpost::GetInstance().GetOreCollectedCount() < 4) {
			Outpost::GetInstance().RemoveAllOreScrollObjects();
			Outpost::GetInstance().CreateOreAtRandomSatellite();
			Outpost::GetInstance().AddToScore(250);
		}
		else {
			Outpost::GetInstance().ProgressToNextLevel();
		}


	}

	/* Test Alien, Bullet or Asteroid to damage the Outpost Core - ending the game */
	if (orxString_Compare(colliderName, "Asteroid") == 0 ||
		orxString_Compare(colliderName, "AlienBullet") == 0 ||
		orxString_Compare(colliderName, "Alien") == 0 ||
		orxString_Compare(colliderName, "Seeker") == 0
		) {

		if (orxString_SearchString(_zPartName, "Outpost") != orxNULL) {
			_poCollider->SetLifeTime(0);

			orxVECTOR shipPosition = orxVECTOR_0;
			this->GetPosition(shipPosition, orxTRUE);

			orxOBJECT *explosion = orxObject_CreateFromConfig("MajorExplosion");
			orxObject_SetPosition(explosion, &shipPosition);

			Outpost::GetInstance().SetGameOver();
		}
	}

	// Add flash effect
	//AddFX("FX-Flash");
	int here = 1;

	return true;
}