#include "Alien.h"

orxOBJECT *lookAheadTest;
orxOBJECT *lookAheadTest2;

void Alien::OnCreate()
{
	orxVECTOR newSafePosition = orxVECTOR_0;
	orxVECTOR outpostPosition = orxVECTOR_0;

	orxFLOAT randomRotation = orxMath_GetRandomFloat(0, 359);
	newSafePosition.fX = orxMath_GetRandomFloat(250, 1600);
	orxVector_2DRotate(&newSafePosition, &newSafePosition, randomRotation);

	outpostPosition = Outpost::GetInstance().GetCurrentOutpostPosition();

	newSafePosition.fX += outpostPosition.fX; //add outpost X
	newSafePosition.fY += outpostPosition.fY; //add outpost Y

	this->SetPosition(newSafePosition, orxTRUE);

	currentLevel = Outpost::GetInstance().GetLevelFromConfig();

	ScrollObject *spawnerSwitch = this->GetOwnedChild();
	if (spawnerSwitch != orxNULL) {
		spawnerSwitch->Enable(orxFALSE);
	}

	//lookAheadTest = orxObject_CreateFromConfig("Test");
	//lookAheadTest2 = orxObject_CreateFromConfig("Test");

}

void Alien::OnDelete()
{
	// Do nothing when deleted
}


void Alien::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (Outpost::GetInstance().IsGameEnabled() == orxFALSE) {
		return;
	}


	const orxDOUBLE ROTATION = (0.015 * 60) * _rstInfo.fDT + (0.0015 * 60 * _rstInfo.fDT * (orxDOUBLE)currentLevel); //0.03;// 0.0075; // 0.015;
	//orxLOG("Rotation %f", ROTATION);

	orxOBJECT *alienObject = this->GetOrxObject();

	float rotation = this->GetRotation(orxTRUE);
	orxVECTOR relativeSpeed = orxVECTOR_0;
	orxObject_GetRelativeSpeed(alienObject, &relativeSpeed);

	//Get Alien Direction
	orxVECTOR alienDirection = orxVECTOR_0;
	alienDirection.fX = orxMath_Cos(rotation);
	alienDirection.fY = orxMath_Sin(rotation);

	//Get Alien position
	orxVECTOR alienPosition = orxVECTOR_0;
	//orxVECTOR mousePos = orxVECTOR_0;
	//orxMouse_GetPosition(&mousePos);
	//orxObject_SetWorldPosition(alien, &mousePos);

	GetPosition(alienPosition, orxTRUE);
	
	orxVECTOR alienLookAheadDirection = orxVECTOR_0;
	orxVECTOR alienLookAheadDirection2 = orxVECTOR_0;
	orxVector_Mulf(&alienLookAheadDirection, &alienDirection, 150);
	alienLookAheadDirection2 = alienLookAheadDirection;

	orxVector_2DRotate(&alienLookAheadDirection, &alienLookAheadDirection, (orxMATH_KF_DEG_TO_RAD * 230));
	orxVector_2DRotate(&alienLookAheadDirection2, &alienLookAheadDirection2, (orxMATH_KF_DEG_TO_RAD * 300));
	
	orxVector_Add(&alienLookAheadDirection, &alienPosition, &alienLookAheadDirection);
	orxVector_Add(&alienLookAheadDirection2, &alienPosition, &alienLookAheadDirection2);

	//orxObject_SetPosition(lookAheadTest, &alienLookAheadDirection);
	//orxObject_SetPosition(lookAheadTest2, &alienLookAheadDirection2);

	orxVECTOR pvContact = orxVECTOR_0;
	orxVECTOR pvNormal = orxVECTOR_0;

	//if there is a raycast, likely collision with an asteroid take evasive manuover, otherwise seek outpost.
	orxOBJECT* objectToAvoid = orxObject_Raycast(&alienPosition,
		&alienLookAheadDirection,
		AlienSelfFlag,
		AsteroidCheckMask,
		orxTRUE,
		&pvContact,
		&pvNormal
	);

	orxOBJECT* objectToAvoid2 = orxObject_Raycast(&alienPosition,
		&alienLookAheadDirection2,
		AlienSelfFlag,
		AsteroidCheckMask,
		orxTRUE,
		&pvContact,
		&pvNormal
	);

	if (objectToAvoid != orxNULL) {

		rotation += (orxFLOAT)ROTATION *5;
		SetRotation(rotation, orxTRUE);
		orxObject_SetRelativeSpeed(alienObject, &relativeSpeed);

		return;
	}

	if (objectToAvoid2 != orxNULL) {

		rotation -= (orxFLOAT)ROTATION * 5;
		SetRotation(rotation, orxTRUE);
		orxObject_SetRelativeSpeed(alienObject, &relativeSpeed);

		return;
	}

	//Get last known Position Vector of Block
	orxVECTOR lastKnownOutpostPosition = orxVECTOR_0;
	lastKnownOutpostPosition = Outpost::GetInstance().GetLastKnownOutpostPosition();

	//Get real Position Vector of Block, used to popen fire when close
	orxVECTOR realOutpostPosition = orxVECTOR_0;
	realOutpostPosition = Outpost::GetInstance().GetCurrentOutpostPosition();

	//Get real Direction to fire on outpost
	orxVECTOR realDirectionToOutpost = orxVECTOR_0;
	orxVector_Sub(&realDirectionToOutpost, &realOutpostPosition, &alienPosition);

	//Length of distance
	orxFLOAT distance = orxVector_GetSize(&realDirectionToOutpost);
	Outpost::GetInstance().ReportAlienDistance(distance);

	ScrollObject *guns = this->GetOwnedChild();
	if (guns != orxNULL) {
		//orxLOG("NAME: %s DISTANCE: %f", guns->GetModelName(), distance);
		guns->Enable(distance < 600, orxFALSE);
	}

	//Get Direction to face outpost
	orxVECTOR directionToOutpost = orxVECTOR_0;
	orxVector_Sub(&directionToOutpost, &lastKnownOutpostPosition, &alienPosition);

	//calculate radians of the final direction
	orxFLOAT radiansToOutpost = 0;
	radiansToOutpost = orxMath_ATan(directionToOutpost.fY, directionToOutpost.fX);

	//trasnlate negative angles .. not nescessary
	orxFLOAT degrees = (orxMATH_KF_RAD_TO_DEG *radiansToOutpost) + 90;
	if (degrees < 0) {
		degrees = 360 + degrees;
	}

	orxFLOAT alienDegrees = orxMath_Mod(orxMATH_KF_RAD_TO_DEG *rotation, 360);
	//if (alienDegrees < 0) {
	//	alienDegrees = 360 + alienDegrees;
	//}

	//orxLOG(" radiansToPost %f degrees %f alien: %f", radiansToOutpost, degrees, alienDegrees);

	if (alienDegrees > degrees) {
		rotation -= (orxFLOAT)ROTATION;
		SetRotation(rotation, orxTRUE);
		//orxObject_SetRotation(alien, rotation);
		orxObject_SetRelativeSpeed(alienObject, &relativeSpeed);
	}
	else {
		rotation += (orxFLOAT)ROTATION;
		SetRotation(rotation, orxTRUE);
		//orxObject_SetRotation(alien, rotation);
		orxObject_SetRelativeSpeed(alienObject, &relativeSpeed);
	}


}

orxBOOL Alien::OnCollide(ScrollObject *_poCollider, 
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (
			orxString_SearchString(colliderName, "Shield") != orxNULL ||
			orxString_SearchString(colliderName, "Top") != orxNULL ||
			orxString_SearchString(colliderName, "Right") != orxNULL ||
			orxString_SearchString(colliderName, "Bottom") != orxNULL ||
			orxString_SearchString(colliderName, "Left") != orxNULL ||
			orxString_SearchString(colliderName, "Outpost") != orxNULL
		)
		{

		orxVECTOR alienPosition = orxVECTOR_0;
		this->GetPosition(alienPosition, orxTRUE);

		orxOBJECT *explosion = orxObject_CreateFromConfig("AlienExplosion");
		orxObject_SetPosition(explosion, &alienPosition);

		Outpost::GetInstance().AddToScore(50);

		if (orxString_SearchString(colliderName, "Shield") == orxNULL) {
			//_poCollider->SetLifeTime(0);
		}
		SetLifeTime(0);
	}


	//Outpost::GetInstance().AddToScore(150);

	//const orxSTRING colliderName = _poCollider->GetModelName();
	//if (orxString_Compare(colliderName, "Laser") == 0) {
	//	_poCollider->SetLifeTime(0);
	//}

	return orxTRUE;
}