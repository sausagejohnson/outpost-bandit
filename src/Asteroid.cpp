#include "Asteroid.h"

void Asteroid::OnCreate()
{
}

void Asteroid::OnDelete()
{
	// Do nothing when deleted
}


void Asteroid::Update(const orxCLOCK_INFO &_rstInfo)
{

}

orxBOOL Asteroid::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName,
	const orxSTRING _zColliderPartName,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();

	orxVECTOR colliderPosition = orxVECTOR_0;
	_poCollider->GetPosition(colliderPosition, orxTRUE);

	//orxVECTOR asteroidPosition = orxVECTOR_0;
	//this->GetPosition(asteroidPosition, orxTRUE);

	orxOBJECT *asteroidExplosion = orxObject_CreateFromConfig("AsteroidExplosion");
	orxObject_SetPosition(asteroidExplosion, &_rvPosition);

	if (orxString_Compare(colliderName, "AlienBullet") == 0)
	{
		orxOBJECT *explosion = orxObject_CreateFromConfig("BulletSplatter");
		orxObject_SetPosition(explosion, &colliderPosition);
		_poCollider->SetLifeTime(0);
	}

	if (orxString_Compare(colliderName, "Alien") == 0)
	{
		orxOBJECT *explosion = orxObject_CreateFromConfig("AlienExplosion");
		orxObject_SetPosition(explosion, &colliderPosition);
		_poCollider->SetLifeTime(0);
	}
		

		

		//MyGame::GetInstance().AddToScore(150);

		//if (orxString_SearchString(colliderName, "Shield") == orxNULL) {
			//_poCollider->SetLifeTime(0);
		//}
	//}


	//MyGame::GetInstance().AddToScore(150);

	//const orxSTRING colliderName = _poCollider->GetModelName();
	//if (orxString_Compare(colliderName, "Laser") == 0) {
	//	_poCollider->SetLifeTime(0);
	//}

	return orxTRUE;
}