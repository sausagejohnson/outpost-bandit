
#include "Outpost.h"

//! MyGame class
class Shield : public ScrollObject
{
public:
	virtual void SetActive(orxBOOL active);
	virtual orxBOOL IsActive();

private:
	orxFLOAT movementSpeed = 0;
	orxBOOL active = orxFALSE; //is this the one to show and flash?

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider, 
		const orxSTRING _zPartName, 
		const orxSTRING _zColliderPartName, 
		const orxVECTOR &_rvPosition, 
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);

};
