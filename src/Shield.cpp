#include "Shield.h"

void Shield::OnCreate()
{
	//// Set initial movement direction
	//m_direction = SOUTH;
	//// Get movement speed from config value 
	//m_movementSpeed = orxConfig_GetFloat("MovementSpeed");
	//// Get direction change interval from config value
	//m_directionChangeInterval = orxConfig_GetFloat("DirectionChangeInterval");
	//movementSpeed = 400;
}

void Shield::OnDelete()
{
	// Do nothing when deleted
}

void Shield::SetActive(orxBOOL active){
	active = false;
}

orxBOOL Shield::IsActive(){
	return active;
}

void Shield::Update(const orxCLOCK_INFO &_rstInfo)
{
	// Always initialize thy variables
	/*orxVECTOR speed = orxVECTOR_0;
	orxVECTOR position = orxVECTOR_0;
	GetPosition(position, orxTRUE);

	if (orxInput_IsActive("MoveLeft") && position.fX > -350) {
		speed.fX = -movementSpeed;
	}

	if (orxInput_IsActive("MoveRight") && position.fX < 350) {
		speed.fX = movementSpeed;
	}

	for (orxOBJECT *pstChild = orxObject_GetOwnedChild(GetOrxObject());
		pstChild;
		pstChild = orxObject_GetOwnedSibling(pstChild))
	{
		const orxSTRING name = orxObject_GetName(pstChild);
		if (orxString_Compare(name, "ShipSpawnerSwitch") == 0) {
			orxObject_Enable(pstChild, orxInput_IsActive("Fire"));
		}
	}

	SetSpeed(speed);*/

}

orxBOOL Shield::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_Compare(colliderName, "AlienBullet") == 0 //||
		//orxString_Compare(colliderName, "Alien") == 0 
		) {
		orxVECTOR position = orxVECTOR_0;
		_poCollider->GetPosition(position, orxTRUE);

		orxOBJECT *explosion = orxObject_CreateFromConfig("BulletSplatter");
		orxObject_SetPosition(explosion, &position);

		Outpost::GetInstance().AddToScore(70);

		_poCollider->SetLifeTime(0);

	}
	if (orxString_Compare(colliderName, "Alien") == 0 
		) {
		orxVECTOR position = orxVECTOR_0;
		_poCollider->GetPosition(position, orxTRUE);

		orxOBJECT *explosion = orxObject_CreateFromConfig("AlienExplosion");
		orxObject_SetPosition(explosion, &position);

		Outpost::GetInstance().AddToScore(70);

		_poCollider->SetLifeTime(0);

	}

	// Add flash effect
	//AddFX("FX-Flash");
	int here = 1;

	return true;
}