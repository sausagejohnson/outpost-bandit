/*
This class keeps an array of 10 distance slots that
cycle and overridden. The smallest value is the one
retrieved.
*/

const int count = 10;

class DistanceEngine 
{
public:
	float GetShortestDistance();
	void ReportDistance(float distance);
	DistanceEngine();

private:
	int currentIndex = 0;
	float distances[count];

};
