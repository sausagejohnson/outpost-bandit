
#include "Outpost.h"

//! MyGame class
class Ship : public ScrollObject
{
public:
	//virtual void CreateCameraShake();


private:
	const int MAX_CHILD_COUNT = 28;

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider,
		const orxSTRING _zPartName,
		const orxSTRING _zColliderPartName,
		const orxVECTOR &_rvPosition,
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);
	virtual orxSTRING PartToObjectName(const orxSTRING orxPartName);
	virtual void DeleteChildByName(const orxSTRING childName);
	virtual void DeleteBodyPartByName(const orxSTRING partName);
	virtual orxVECTOR GetChildPositionByName(const orxSTRING childName);
	virtual ScrollObject* GetChildByName(const orxSTRING childName);
	virtual int ChildCount();
	virtual void UpdateMass();

	//orxOBJECT *cameraHandle;
};
