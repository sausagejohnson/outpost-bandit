
#include "Outpost.h"

class SplashScreen : public ScrollObject
{
public:

private:
	orxU16 AsteroidCheckMask = 32;

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);
	virtual void	CheckForAnyKeyOrJoystickButton();
};
