
#include "Outpost.h"

#define MAP_INDEX_FOR_KEY 0
#define MAP_INDEX_FOR_JOY 1

typedef struct mapping_t {
	orxCHAR bindName[50] = {};
	orxCHAR keyBinding[50] = {};
	orxCHAR joystickBinding[50] = {};
} Mapping;

class ControlMenu : public ScrollObject
{
public:
	virtual void Up();
	virtual void Down();
	virtual void Hide();
	virtual void Show();
	virtual void	StartRemapInput();
	virtual void	StopRemapInput();
	virtual orxBOOL	IsRemapping();
	virtual orxSTRING GetDirectionKeysForDisplay();
	virtual orxSTRING GetShieldKeysForDisplay();
	virtual orxBOOL IsEqualTo(orxSTRING stringA, orxSTRING stringB);

private:
	int selectedMenuIndex = 0;
	int menuTotalCount = 0; //will be calculated on the fly
	orxBOOL isRemapping = orxFALSE;

	orxOBJECT *highlight;

	Mapping controlMappings[50];
	orxOBJECT *mappingRowObject[50];
	orxOBJECT *mappingNameObject[50];
	orxOBJECT *keyTextObject[50];
	orxOBJECT *joyTextObject[50];

	orxVECTOR origin = orxVECTOR_0;// { -350, -232, -0.1 };
	orxVECTOR gap = orxVECTOR_0; //{ 50, 26, 0};

	orxCHAR directionKeysForDisplay[50] = {}; //no good for vs2015
	orxCHAR shieldKeysForDisplay[50] = {}; //no good for vs2015

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider,
		const orxSTRING _zPartName,
		const orxSTRING _zColliderPartName,
		const orxVECTOR &_rvPosition,
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);
	virtual void	UpdateHighlightRow();
	virtual void	UpdateTextObjectsFromMapping();
	virtual void	SaveMappingToDisk();
	//virtual void	RebindAllMappingInTableByBindingName(const orxSTRING bindingName);
	virtual void 	UnbindAllIndexesByName(orxSTRING bindingName);
	virtual void 	RemoveOveriddenBindings(); //remove older bindings, keep overidden
	virtual void 	RealignMappingIndexes(); //keep keys on index 0 and joys on index 1
	virtual void	DebugAllBindings(); //output all current bindings
	virtual orxBOOL IsAnAnalogueBindName(const orxSTRING bindingName);
	virtual orxBOOL IsUpDownLeftOrRight(const orxSTRING bindingName);
	virtual orxU32  GetStringLengthEOL(const orxSTRING _zString);

};
