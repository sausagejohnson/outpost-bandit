#include "SplashScreen.h"

void SplashScreen::OnCreate()
{
	//outpostPosition = Outpost::GetInstance().GetCurrentOutpostPosition();
}

void SplashScreen::OnDelete()
{
	Outpost::GetInstance().InitGame();
}

void SplashScreen::CheckForAnyKeyOrJoystickButton(){
	orxINPUT_TYPE inputType;
	orxENUM buttonID;
	orxFLOAT value;
	
	if (orxInput_GetActiveBinding(&inputType, &buttonID, &value)) {
		if (inputType == orxINPUT_TYPE_JOYSTICK_BUTTON || inputType == orxINPUT_TYPE_KEYBOARD_KEY) { 
			
			const orxSTRING val = orxInput_GetBindingName(inputType, buttonID, orxINPUT_MODE_FULL);
			//gives key_space
			
			orxOBJECT *screen = this->GetOrxObject();
 			orxObject_SetAlpha(screen, 0.0);
			
			this->SetLifeTime(0);
			
			const orxSTRING pressedBinding = orxInput_GetBindingName(inputType, buttonID, orxINPUT_MODE_FULL);
			orxInput_ResetValue(pressedBinding);
		}
	}
}

void SplashScreen::Update(const orxCLOCK_INFO &_rstInfo)
{
	CheckForAnyKeyOrJoystickButton();
}

