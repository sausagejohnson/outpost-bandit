
#define __SCROLL_IMPL__
#include "Outpost.h"
#include "Alien.h"
#include "Seeker.h"
#include "Ship.h"
#include "Shield.h"
#include "SplashScreen.h"
#include "Asteroid.h"
#include "ControlMenu.h"
//#include "Bullet.h"
#undef __SCROLL_IMPL__

#define OUTPOST_GAME_ICON  1

orxOBJECT *scene;
orxCAMERA *camera;
ScrollObject *outpost;
Shield *edgeShield;
Shield *cornerShield;
ScrollObject *splashScreen;
ControlMenu *remapControlMenu;
orxVECTOR lastKnownOutpostPosition = orxVECTOR_0;
int score = 0;
int highScore = 0;
orxFLOAT shieldPower = 40;
orxFLOAT bonus = 100;
const int ALIEN_POINTS = 120;
const int BLAST_POINTS = 50;
const orxFLOAT MAX_SHIELD_POWER = 40;
orxOBJECT *scoreObject;
orxOBJECT *shieldTextObject;
orxOBJECT *shieldBarObject;
orxOBJECT *bonusTextObject;
orxOBJECT *bonusTextValueObject;
orxOBJECT *bonusBarObject;
orxOBJECT *levelTextObject;
orxOBJECT *satellites;

orxOBJECT *titleScene;
orxOBJECT *controllerIcon;

orxOBJECT *cameraHandle;

orxBOOL controlsEnabled = orxFALSE;
orxBOOL gameEnabled = orxFALSE;
orxBOOL titleEnabled = orxFALSE;

orxBOOL bDebugLevelBackup;

int lastKnownEdgeOrCorner = -1;
int oreCollected = 0;
//orxFLOAT shortestAlienDistance = 0;

const int LEVEL_SECONDS = 30;
int levelCountdown = LEVEL_SECONDS;

orxCLOCK *levelCountdownClock;
orxCLOCK *bonusCountdownClock;

void Outpost::BindObjects()
{
	ScrollBindObject<Ship>("Outpost");
	ScrollBindObject<Alien>("Alien");
	ScrollBindObject<Seeker>("Seeker");
	ScrollBindObject<Shield>("EdgeShield");
	ScrollBindObject<Shield>("CornerShield");
	ScrollBindObject<Asteroid>("Asteroid");
	ScrollBindObject<ControlMenu>("ControlMenu");
	ScrollBindObject<SplashScreen>("SplashScreen");
}

void Outpost::AddOreCollected(int count) {
	oreCollected += count;
}

int Outpost::GetOreCollectedCount(){
	return oreCollected;
}

void Outpost::AddToScore(int points) {
	score += points;
	
	UpdateScoreDisplay();
}

//void Outpost::SetSpeedIndicator(orxFLOAT distance) {
//	orxCHAR formattedSpeed[50];
//	orxString_Print(formattedSpeed, "%f", distance);
//	orxObject_SetTextString(shieldTextObject, formattedSpeed);
//}

void Outpost::LoseControl() {
	controlLossTimeout = 120;
}


void orxFASTCALL SampleOutpostPosition(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
	if (controlsEnabled == orxFALSE) {
		return;
	}
	outpost->GetPosition(lastKnownOutpostPosition, orxTRUE);
}

void Outpost::PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (orxObject_GetLastAddedSound(object) != orxNULL) {
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
}

void Outpost::SwitchToTitleMusic() {
	orxU32 masterBusID = orxSound_GetMasterBusID();
	orxU32 childBusID = orxSound_GetBusChild(masterBusID);

	orxSound_SetBusVolume(childBusID, 0.7); //title

	childBusID = orxSound_GetBusSibling(childBusID);

	orxSound_SetBusVolume(childBusID, 0.0); //ingame

}

void Outpost::SwitchToGameMusic() {
	orxU32 masterBusID = orxSound_GetMasterBusID();
	orxU32 childBusID = orxSound_GetBusChild(masterBusID);

	orxSound_SetBusVolume(childBusID, 0.0); //title

	childBusID = orxSound_GetBusSibling(childBusID);

	orxSound_SetBusVolume(childBusID, 0.7); //ingame
}

orxVECTOR Outpost::GetLastKnownOutpostPosition()
{
	return lastKnownOutpostPosition;
}

orxVECTOR Outpost::GetCurrentOutpostPosition()
{
	orxVECTOR outpostPosition = orxVECTOR_0;
	outpost->GetPosition(outpostPosition, orxTRUE);
	return outpostPosition;
}

orxBOOL Outpost::GetControlsEnabled() {
	return controlsEnabled;
}

orxBOOL Outpost::IsGameEnabled() {
	return gameEnabled;
}

void Outpost::MoveCameraScrollStars() {
	orxVECTOR cameraPos = orxVECTOR_0;
	orxCAMERA *camera = this->GetMainCamera();
	orxCamera_GetPosition(camera, &cameraPos);

	cameraPos.fX -= 1;

	orxCamera_SetPosition(camera, &cameraPos);
}

void Outpost::RemoveAllOreScrollObjects() {

	for (orxOBJECT *satellite = orxObject_GetOwnedChild(satellites); satellite; satellite = orxObject_GetOwnedSibling(satellite)) {
		const orxSTRING satName = orxObject_GetName(satellite);

		if (orxString_SearchString(satName, "Satellite") != orxNULL) {

			for (orxOBJECT *child = orxObject_GetOwnedChild(satellite); child; child = orxObject_GetOwnedSibling(child)) {
				const orxSTRING testName = orxObject_GetName(child);
				if (orxString_Compare(testName, "Ore") == 0) {
					//orxLOG("Child here: %s", testName);
					orxObject_SetLifeTime(child, 0);
				}
			}

		}

	}

}

void Outpost::CreateOreAtRandomSatellite() {
	int random = orxMath_GetRandomU32(0, 3);

	orxOBJECT *satellite = orxObject_GetOwnedChild(satellites);

	if (random > 0) {
		for (int x = 0; x < random; x++) {
			satellite = orxObject_GetOwnedSibling(satellite);
		}
	}

	if (satellite != orxNULL) {
		//orxOBJECT *ore = orxObject_CreateFromConfig("Ore");
		ScrollObject *scrollOre = CreateObject("Ore");
		orxOBJECT *ore = scrollOre->GetOrxObject();

		orxVECTOR satellitePos = orxVECTOR_0;
		orxObject_GetWorldPosition(satellite, &satellitePos);
		satellitePos.fX += 15;

		orxObject_SetWorldPosition(ore, &satellitePos);
		orxObject_Attach(ore, satellite);
		orxObject_SetOwner(ore, satellite);

	}


}

void Outpost::ApplyShieldPosAndRotToOutpost(float extraRotation)
{
	int cornerOrEdge = 0;
	if (edgeShield->IsActive()){
		cornerOrEdge = 1;
	}
	
	orxVECTOR outpostPos = orxVECTOR_0;
	outpost->GetPosition(outpostPos, orxTRUE);
	orxFLOAT outpostRot = outpost->GetRotation(orxTRUE);
	outpostRot += (extraRotation * orxMATH_KF_DEG_TO_RAD);

	orxOBJECT *edgeShieldObject = edgeShield->GetOrxObject();
	orxOBJECT *cornerShieldObject = cornerShield->GetOrxObject();
	orxObject_SetPosition(edgeShieldObject, &outpostPos);
	orxObject_SetRotation(edgeShieldObject, outpostRot);
	orxObject_SetPosition(cornerShieldObject, &outpostPos);
	orxObject_SetRotation(cornerShieldObject, outpostRot);
}

orxSTATUS orxFASTCALL EventHandler(const orxEVENT *_pstEvent)
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	// Depending on type
	switch (_pstEvent->eType)
	{
	case orxEVENT_TYPE_SHADER:
	{
		// Param?
		if (_pstEvent->eID == orxSHADER_EVENT_SET_PARAM)
		{
			orxSHADER_EVENT_PAYLOAD *pstPayload;

			// Gets payload
			pstPayload = (orxSHADER_EVENT_PAYLOAD *)_pstEvent->pstPayload;

			// Camera position?
			if (!orxString_Compare(pstPayload->zParamName, "CameraPos"))
			{
				pstPayload->vValue.fZ = 0;
				orxVECTOR cameraPosition;
				orxObject_GetWorldPosition(cameraHandle, &cameraPosition);

				pstPayload->vValue.fX = cameraPosition.fX;
				pstPayload->vValue.fY = -cameraPosition.fY;
				pstPayload->vValue.fZ = 0;

			}
		}

		break;
	}

	default:
	{
		break;
	}
	}

	// Done!
	return eResult;
}
//
//orxSTATUS orxFASTCALL ObjectEventHandler(const orxEVENT *_pstEvent)
//{
//	orxSTATUS eResult = orxSTATUS_SUCCESS;
//
//	// Param?
//	if (_pstEvent->eID == orxOBJECT_EVENT_DELETE)
//	{
//		orxLOG("here");
//		//orxSHADER_EVENT_PAYLOAD *pstPayload;
//
//		// Gets payload
//		//pstPayload = (orxSHADER_EVENT_PAYLOAD *)_pstEvent->pstPayload;
//
//		// Camera position?
////		if (!orxString_Compare(pstPayload->zParamName, "CameraPos"))
////		{
////		
////		}
//	}
//	// Done!
//	return eResult;
//}

orxOBJECT* Outpost::GetChildObjectFromScene(orxSTRING childName) {
	for (orxOBJECT *pstChild = orxObject_GetOwnedChild(scene);
		pstChild;
		pstChild = orxObject_GetOwnedSibling(pstChild))
		{
			const orxSTRING name = orxObject_GetName(pstChild);
			if (orxString_Compare(name, childName) == 0) {
				return pstChild;
			}
		}

	return orxNULL;
}

/* Replace GetObjectByName with this */
orxOBJECT* Outpost::FindObjectByName(orxSTRING nameOfObjectToFind) {

	orxOBJECT *pstObject = orxObject_GetNext(orxNULL, orxSTRINGID_UNDEFINED);
	
	for (orxOBJECT *pstNext = pstObject;
		pstNext;
		pstNext = orxObject_GetNext(pstNext, orxSTRINGID_UNDEFINED))
	{
		const orxSTRING name = orxObject_GetName(pstNext);
		//orxLOG("Object: %s", name);
		if (orxString_Compare(name, "HighScore") == 0){
			int here = 1;
		}
		if (orxString_Compare(name, nameOfObjectToFind) == 0) {
			return pstNext;
		}
	}

	return orxNULL;
}

orxOBJECT* Outpost::GetObjectByName(orxSTRING nameOfObjectToFind) {

	orxOBJECT *pstObject = orxOBJECT(orxStructure_GetFirst(orxSTRUCTURE_ID_OBJECT));

	for (orxOBJECT *pstChild = orxObject_GetOwnedChild(pstObject);
		pstChild;
		pstChild = orxOBJECT(orxStructure_GetNext(pstChild)))
	{
		const orxSTRING name = orxObject_GetName(pstChild);
		if (orxString_Compare(name, "HudRadar") == 0){
			int here = 1;
		}
		if (orxString_Compare(name, nameOfObjectToFind) == 0) {
			return pstChild;
		}
	}

	return orxNULL;
}

void Outpost::CreateLevelBanner() {
	orxOBJECT *banner = GetChildObjectFromScene("LevelBanner");
	if (banner != orxNULL) {
		orxObject_SetLifeTime(banner, 0);
	}

	CreateOrxObjectToScene("LevelBanner");
}

void Outpost::SetStartGame() {
	ShowGameScene();
	outpost->SetLifeTime(0); //reset outpost so the ship is full again
	outpost = CreateScrollObjectToScene("Outpost");
	orxObject_SetParent(cameraHandle, outpost->GetOrxObject());

	SetLevelInConfig(GetLevelFromConfig());
	CreateLevelBanner();

	oreCollected = 0;
	
	score = 0;
	UpdateScoreDisplay();
	
	ResetBonus();
	StartLevelCountdown();
	
}

/* Called only once. Use EnableGameScene */
void Outpost::ShowGameScene() {
	if (gameEnabled == orxFALSE) {
		gameEnabled = orxTRUE;
		orxCamera_SetPosition(camera, &orxVECTOR_0);
		controlsEnabled = orxTRUE;
		controlLossTimeout = 0;
		orxObject_EnableRecursive(scene, orxTRUE);

		orxVECTOR resetPosition = { -200, 0, 0.5 };
		outpost->SetPosition(resetPosition, orxTRUE);

		RemoveTitle();
		DeleteControllerIcon();
		CreateAsteroids();
		RecreateAlienLauncher();
		RecreateAsteroidCreator();

		RemoveAllOreScrollObjects();
		CreateOreAtRandomSatellite();

		SwitchToGameMusic();
	}
}
void Outpost::HideGameScene() {
	if (gameEnabled == orxTRUE) {
		gameEnabled = orxFALSE;
		orxObject_EnableRecursive(scene, orxFALSE);
		
		DeleteAgedAsteroids();
	}
}

orxBOOL orxFASTCALL MySaveFilterCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// Return orxTRUE for the section "SaveSettings", orxFALSE otherwise
	// -> filters out everything but the "SaveSettings" section
	return (
		orxString_Compare(_zSectionName, "GameInfo") == 0
		) ? orxTRUE : orxFALSE;
}

void Outpost::SaveHighScoreToDisk(){
	orxConfig_PushSection("GameInfo");
	orxConfig_SetU32("HighScore", highScore);
	orxConfig_PopSection();
	
	orxBOOL bEncrypt = orxFALSE;
	orxConfig_Save("outpost-highscore.ini", bEncrypt, MySaveFilterCallback);
}

void Outpost::SetHighScoreText(){
	orxCHAR formattedHighScore[20];
	orxString_Print(formattedHighScore, "%d", highScore);
	
	orxOBJECT *highScoreObject = FindObjectByName("HighScore");
	orxObject_SetTextString(highScoreObject, formattedHighScore);
}

void Outpost::UpdateHighScore(){
	if (score > highScore){
		highScore = score;
		SaveHighScoreToDisk();
	}

	SetHighScoreText();
}

void Outpost::SetGameOver() {
	StopBonusCountdown();

	cornerShield->Enable(orxFALSE);	
	edgeShield->Enable(orxFALSE);

	DeleteCurrentAlienLauncher();
	DeleteCurrentAsteroidCreator();

	SetLevelInConfig(1);
	UpdateGameLevelDetailsInConfig();
	HideGameScene();
	CreateTitle();
	UpdateHighScore();
	UpdateKeyInstructionsText();

	DisableControls();
	ResetZoom();
	//orxCAMERA *camera = this->GetMainCamera();
	//orxVECTOR neutral = orxVECTOR_0;
	//neutral.fX = -0;
	//neutral.fZ = -2;
	//orxCamera_SetPosition(camera, &neutral); //ensure z doesn't cause issues

	SwitchToTitleMusic();
}

void Outpost::CreateTitle(){
	if (titleEnabled == orxFALSE) {
		titleScene = orxObject_CreateFromConfig("TitleScene");
		//controllerIcon = GetObjectByName("Controller");
		//CreateControllerIcon();
		titleEnabled = orxTRUE;
		//orxObject_CreateFromConfig("SineObject");
	}
}
void Outpost::RemoveTitle() {
	if (titleEnabled == orxTRUE) {
		orxObject_SetLifeTime(titleScene, 0);
		//DeleteControllerIcon();
		titleEnabled = orxFALSE;
	}
}

void Outpost::CreateControllerIcon(){
	controllerIcon = GetObjectByName("Controller");
	//orxObject_Enable(controllerIcon, orxFALSE);
	//orxCAMERA *camera = this->GetMainCamera();
	//orxObject_SetParent(controllerIcon, camera);
}

void Outpost::DeleteControllerIcon(){
	if (controllerIcon != orxNULL){
		orxObject_SetLifeTime(controllerIcon, 0);
		controllerIcon = orxNULL;
	}
}

void Outpost::DisableControls() {
	controlsEnabled = orxFALSE;
}


void Outpost::ShakeCamera() {
//	if (outpost != orxNULL) {
//		Ship *ship = (Ship *)outpost;
//		ship->CreateCameraShake();
//	}
	if (cameraHandle != orxNULL) {
		//orxObject_AddUniqueFX(cameraHandle, )
		orxObject_AddTimeLineTrack(cameraHandle, "CameraShakeTrack");
	}
}

void Outpost::DoRemapMenuInputs()
{
	if (inputMode != REMAPPING) return;

	if (orxInput_HasBeenActivated("Quit")) //close menu
	{
		if (remapControlMenu->IsRemapping()) {
			remapControlMenu->StopRemapInput();
		}
		else {
			remapControlMenu->Hide();
			inputMode = PLAYING;
		}
		UpdateKeyInstructionsText();
	}

	if (!remapControlMenu->IsRemapping()) {
		if (orxInput_HasBeenActivated("GoUp"))
		{
			remapControlMenu->Up();
		}

		if (orxInput_HasBeenActivated("GoDown"))
		{
			remapControlMenu->Down();
		}

		if (!orxInput_IsActive("Select") && orxInput_HasNewStatus("Select"))
		{
			//orxLOG("Remap please");
			remapControlMenu->StartRemapInput();
		}
	}
	
}

void Outpost::DoInputs(const orxCLOCK_INFO &_pstClockInfo)
{
	
	
	if (inputMode != PLAYING) return;

	if (orxInput_HasBeenActivated("Quit") && inputMode == PLAYING)
	{
		if (gameEnabled == orxFALSE) {
			quit = orxTRUE;
		}
		else {
			SetGameOver();
		}
	}

	if (orxInput_IsActive("Test") && orxInput_HasNewStatus("Test")) {
		//orxOBJECT *cameraHandle = GetObjectByName("CameraHandle");
		//orxObject_SetLifeTime(cameraHandle, 0);
		//Ship *ship = (Ship *)outpost->GetOrxObject();
		//ShakeCamera();
        //OutputTestInfoToLog();
	}

	if (orxInput_IsActive("KillScene") && orxInput_HasNewStatus("KillScene")) {
		//HideGameScene();
		//CreateTitle();
	}

	if (orxInput_HasBeenActivated("StartGame") && !IsGameEnabled()) {
		SetStartGame();
	}

	if (orxInput_HasBeenActivated("RemapControls") && !IsGameEnabled()) {
		remapControlMenu->Show();

		inputMode = REMAPPING;
		//orxLOG("Remap open");
	}

	if (controlsEnabled == orxFALSE) {
		return;
	}





	orxOBJECT *outpostObject = outpost->GetOrxObject();
	//orxBODY *body = orxOBJECT_GET_STRUCTURE(outpostObject, BODY);

	orxVECTOR centerWorldPosition = orxVECTOR_0;
	orxVECTOR outpostPosition = orxVECTOR_0;
	orxObject_GetWorldPosition(outpostObject, &outpostPosition);
	orxFLOAT distanceFromCenter = orxVector_GetDistance(&outpostPosition, &centerWorldPosition);

	//SetSpeedIndicator(distanceEngine->GetShortestDistance());

	orxFLOAT force = MAX_FORCE;

	if (distanceFromCenter > 3100) {
		force = ((3800 - distanceFromCenter) / 1000) * (MAX_FORCE * _pstClockInfo.fDT);
		if (force < 1.5) {
			force = 1.5;
		}

		orxOBJECT *hud = GetChildObjectFromScene("HudRadar");
		if (hud != orxNULL){
			orxObject_AddUniqueFX(hud, "HudBlinkFX");
		}
	} else {
		force = MAX_FORCE * _pstClockInfo.fDT;
	}

	//orxLOG("Force %f", force);

	orxVECTOR rightForce = { force, 0, 0 };
	orxVECTOR leftForce = { -force, 0, 0 };
	orxVECTOR upForce = { 0, -force, 0 };
	orxVECTOR downForce = { 0, force, 0 };

	int cornerOrEdge = -1; //0 = corner, 1 = edge
	//float extraRotationToAdd = 0;





	orxVECTOR speedDirection = orxVECTOR_0;
	orxObject_GetSpeed(outpostObject, &speedDirection);

	if (controlLossTimeout > 0) {
		//orxLOG("%f", controlLossTimeout);
		controlLossTimeout = controlLossTimeout - (_pstClockInfo.fDT*60);
	}
	else {

		if (orxInput_IsActive("AnalogLeftRight")) {
			orxVECTOR filteredForceByAnalog = rightForce;
			orxFLOAT analogX = orxInput_GetValue("AnalogLeftRight");

			orxVector_Mulf(&filteredForceByAnalog, &filteredForceByAnalog, analogX );
			orxVector_Add(&speedDirection, &speedDirection, &filteredForceByAnalog );

			orxObject_SetSpeed(outpostObject, &speedDirection);
			if (analogX < -joystickThreshold){
				ApplyNegativeRotation(_pstClockInfo);
			} else if (analogX > joystickThreshold) {
				ApplyPositiveRotation(_pstClockInfo);
			}
			
		} else if (orxInput_IsActive("GoLeft") || orxInput_IsActive("GoRight")){
			
			if (orxInput_IsActive("GoLeft")) {
				orxVector_Add(&speedDirection, &speedDirection, &leftForce);
				orxObject_SetSpeed(outpostObject, &speedDirection);

				ApplyNegativeRotation(_pstClockInfo);
				//orxLOG("Key Speed %f", speedDirection.fX);

			}
			if (orxInput_IsActive("GoRight")) {
				orxVector_Add(&speedDirection, &speedDirection, &rightForce);
				orxObject_SetSpeed(outpostObject, &speedDirection);

				ApplyPositiveRotation(_pstClockInfo);
			}
			
		}

		if (orxInput_IsActive("AnalogUpDown")) {
			orxVECTOR filteredForceByAnalog = upForce;
			orxFLOAT analogY = orxInput_GetValue("AnalogUpDown");

			orxVector_Mulf(&filteredForceByAnalog, &filteredForceByAnalog, analogY );
			orxVector_Sub(&speedDirection, &speedDirection, &filteredForceByAnalog );

			orxObject_SetSpeed(outpostObject, &speedDirection);
			
			if (analogY < -joystickThreshold){
				ApplyNegativeRotation(_pstClockInfo);
			} else if (analogY > joystickThreshold) {
				ApplyPositiveRotation(_pstClockInfo);
			}

		} else if(orxInput_IsActive("GoUp") || orxInput_IsActive("GoDown")){
			
			if (orxInput_IsActive("GoUp")) {
				orxVector_Add(&speedDirection, &speedDirection, &upForce);
				orxObject_SetSpeed(outpostObject, &speedDirection);

				if (!orxInput_IsActive("GoLeft") && !orxInput_IsActive("GoRight")){
					ApplyNegativeRotation(_pstClockInfo);
				}
				
			}
			if (orxInput_IsActive("GoDown")) {
				orxVector_Add(&speedDirection, &speedDirection, &downForce);
				orxObject_SetSpeed(outpostObject, &speedDirection);

				if (!orxInput_IsActive("GoLeft") && !orxInput_IsActive("GoRight")){
					ApplyPositiveRotation(_pstClockInfo);
				}
			}	
		
		}

		

	}




	if (orxInput_IsActive("ShieldUp") && !orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldRight") && !orxInput_IsActive("ShieldDown")) {
		cornerOrEdge = 1;
		extraRotationToAdd = 0;
	}
	if (orxInput_IsActive("ShieldRight") && !orxInput_IsActive("ShieldUp") && !orxInput_IsActive("ShieldDown") && !orxInput_IsActive("ShieldLeft")) {
		cornerOrEdge = 1;
		extraRotationToAdd = 90;
	}
	if (orxInput_IsActive("ShieldDown") && !orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldRight") && !orxInput_IsActive("ShieldUp")) {
		cornerOrEdge = 1;
		extraRotationToAdd = 180;
	}
	if (orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldUp") && !orxInput_IsActive("ShieldDown") && !orxInput_IsActive("ShieldRight")) {
		cornerOrEdge = 1;
		extraRotationToAdd = 270;
	}
	
	
	if (orxInput_IsActive("ShieldUp") && orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldRight") && !orxInput_IsActive("ShieldDown")) {
		cornerOrEdge = 0;
		extraRotationToAdd = 0;
		//orxLOG("Yep");
	}
	if (orxInput_IsActive("ShieldRight") && orxInput_IsActive("ShieldUp") && !orxInput_IsActive("ShieldDown") && !orxInput_IsActive("ShieldLeft")) {
		cornerOrEdge = 0;
		extraRotationToAdd = 90;
	}
	if (orxInput_IsActive("ShieldDown") && orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldRight") && !orxInput_IsActive("ShieldUp")) {
		cornerOrEdge = 0;
		extraRotationToAdd = 270;
	}
	if (!orxInput_IsActive("ShieldLeft") && !orxInput_IsActive("ShieldUp") && orxInput_IsActive("ShieldDown") && orxInput_IsActive("ShieldRight")) {
		cornerOrEdge = 0;
		extraRotationToAdd = 180;
	}

	if (cornerOrEdge != -1 && cornerOrEdge != lastKnownEdgeOrCorner) {
		PlaySoundOn(outpostObject, "Shield");
	}

	if (cornerOrEdge >= 0) {
		ReduceShield(_pstClockInfo);
	} else {
		IncreaseShield(_pstClockInfo);
	}

	//orxLOG("cornerOrEdge %f", shieldPower);
	lastKnownEdgeOrCorner = cornerOrEdge;

	if (shieldPower < 1.0) {
		edgeShield->Enable(orxFALSE, orxFALSE);
		cornerShield->Enable(orxFALSE, orxFALSE);
	}	else {
		edgeShield->Enable(cornerOrEdge == 1, orxFALSE);
		cornerShield->Enable(cornerOrEdge == 0, orxFALSE);
	}

	edgeShield->SetActive(orxFALSE);
	cornerShield->SetActive(orxFALSE);
	if (cornerOrEdge == 0){
		cornerShield->SetActive(orxTRUE);
	}
	//ApplyShieldPosAndRotToOutpost(cornerOrEdge, extraRotationToAdd);

	if (
		orxInput_IsActive("GoUp") == orxFALSE &&
		orxInput_IsActive("GoDown") == orxFALSE &&
		orxInput_IsActive("GoLeft") == orxFALSE &&
		orxInput_IsActive("GoRight") == orxFALSE &&
		orxInput_IsActive("AnalogUpDown") == orxFALSE &&
		orxInput_IsActive("AnalogLeftRight") == orxFALSE
		) {

		float rotation = outpost->GetRotation();
		if (rotation > 0.02) {
			ApplyNegativeRotation(_pstClockInfo);
		}
		else if (rotation < -0.02) {
			ApplyPositiveRotation(_pstClockInfo);
		}

	}

}

void Outpost::ApplyPositiveRotation(const orxCLOCK_INFO &_pstClockInfo){
	orxFLOAT currentRotation = 0;
	currentRotation = outpost->GetRotation(orxTRUE);
	
	if (currentRotation < ROTATION_MAX_DISTANCE) {
		currentRotation += (ROTATION_UNIT * 62.5 * _pstClockInfo.fDT) ;
		outpost->SetRotation(currentRotation, orxTRUE);
	}
}
void Outpost::ApplyNegativeRotation(const orxCLOCK_INFO &_pstClockInfo){
	orxFLOAT currentRotation = 0;
	currentRotation = outpost->GetRotation(orxTRUE);
	
	if (currentRotation > -ROTATION_MAX_DISTANCE) {
		currentRotation -= ROTATION_UNIT * 62.5 * _pstClockInfo.fDT;
		outpost->SetRotation(currentRotation, orxTRUE);
	}
}

void Outpost::UpdateScoreDisplay(){
	orxCHAR formattedScore[50];
	orxString_Print(formattedScore, "%d", score);

	orxObject_SetTextString(scoreObject, formattedScore);
}

void Outpost::UpdateBonusValueDisplay(){
	orxCHAR formattedBonus[50];
	if (bonus == 100){
		orxString_Print(formattedBonus, "%4.0f", bonus);
	} else {
		orxString_Print(formattedBonus, "%4.1f", bonus);
	}
	orxObject_SetTextString(bonusTextValueObject, formattedBonus);
}

void Outpost::ResetBonus() {
	bonus = 100;
	
	orxVECTOR size = { 48, 1, 1 };
	orxObject_SetScale(bonusBarObject, &size);

	UpdateBonusValueDisplay();
}

void Outpost::ReduceBonus() {
	bonus -= 0.15;
	if (bonus < 0) {
		bonus = 0;
	}

	orxFLOAT bonusAsFraction = bonus/100;
	
	orxVECTOR size = { bonusAsFraction*48, 1, 1 };
	orxObject_SetScale(bonusBarObject, &size);

	UpdateBonusValueDisplay();
}

void Outpost::IncreaseShield(const orxCLOCK_INFO &_pstClockInfo) {
	shieldPower += 0.32 * 60 * _pstClockInfo.fDT;
	if (shieldPower > MAX_SHIELD_POWER) {
		shieldPower = MAX_SHIELD_POWER;
	}

	orxVECTOR size = { shieldPower, 1, 1 };
	orxObject_SetScale(shieldBarObject, &size);
}

void Outpost::ReduceShield(const orxCLOCK_INFO &_pstClockInfo) {
	shieldPower -= 0.16 * 60 * _pstClockInfo.fDT;
	if (shieldPower < 0.9) {
		shieldPower = 0.9;
	}

	orxVECTOR size = { shieldPower, 1, 1 };
	orxObject_SetScale(shieldBarObject, &size);
}

void Outpost::DeleteAgedAsteroids() {

	for (orxOBJECT *child = orxObject_GetOwnedChild(scene); child; child = orxObject_GetOwnedSibling(child)) {
		if (orxString_Compare(orxObject_GetName(child), "Asteroid") == 0) {
			orxObject_SetLifeTime(child, 0);
		}
	}
}

void Outpost::CreateAsteroids() {
	int level = GetLevelFromConfig();

	for (int x = 0; x < level * 6; x++) {
		orxVECTOR asteroidPosition = orxVECTOR_0;

		orxFLOAT randomRotation = orxMath_GetRandomFloat(0, 359);
		asteroidPosition.fX = orxMath_GetRandomFloat(350, 3000);
		orxVector_2DRotate(&asteroidPosition, &asteroidPosition, randomRotation);
		asteroidPosition.fX = asteroidPosition.fX - 200; //offcentre it

		orxOBJECT *asteroid = CreateOrxObjectToScene("Asteroid");
		ApplyFadeInToObject(asteroid);
		orxObject_SetPosition(asteroid, &asteroidPosition);
	}
}

ScrollObject* Outpost::CreateScrollObjectToScene(const orxSTRING name) {
	ScrollObject *scrollObject = CreateObject(name);
	orxOBJECT *object = scrollObject->GetOrxObject();
	orxObject_SetOwner(object, scene);

	return scrollObject;
}

void Outpost::ApplyFadeInToObject(orxOBJECT *object){
	orxObject_SetAlpha(object, 0.0);
	orxObject_AddUniqueFX(object, "GeneralFadeInFX");
}

orxOBJECT* Outpost::CreateOrxObjectToScene(const orxSTRING name) {
	orxOBJECT *object = orxObject_CreateFromConfig(name);
	orxObject_SetOwner(object, scene);

	return object;
}

void Outpost::CreateGameObjects() {
	scene = orxObject_CreateFromConfig("Scene");

	outpost = CreateScrollObjectToScene("Outpost");

	//outpost->Enable(orxTRUE);
	scoreObject = CreateOrxObjectToScene("Score");
	shieldTextObject = CreateOrxObjectToScene("ShieldTextObject");
	shieldBarObject = CreateOrxObjectToScene("ShieldBar");
	levelTextObject = CreateOrxObjectToScene("LevelTextObject");
	
	bonusTextObject = CreateOrxObjectToScene("BonusTextObject");
	bonusTextValueObject = CreateOrxObjectToScene("BonusTextValueObject");
	bonusBarObject = CreateOrxObjectToScene("BonusBar");

	//edgeShield = CreateScrollObjectToScene("EdgeShield");
	//fcornerShield = CreateScrollObjectToScene("CornerShield");

	edgeShield = CreateObject<Shield>("EdgeShield");
	cornerShield = CreateObject<Shield>("CornerShield");
	
	edgeShield->Enable(orxFALSE, orxFALSE);
	cornerShield->Enable(orxFALSE, orxFALSE);

	orxObject_SetParent(edgeShield->GetOrxObject(), outpost->GetOrxObject());
	orxObject_SetParent(cornerShield->GetOrxObject(), outpost->GetOrxObject());
	
	satellites = CreateOrxObjectToScene("Satellites");

	////Precreate asteroids before the spawner starts it's job
	////CreateAsteroids();

	//CreateOreAtRandomSatellite();

	//orxOBJECT *outpostObject = outpost->GetOrxObject();
	//PlaySoundOn(scene, "ThemeMusic");

	orxObject_EnableRecursive(scene, orxFALSE);
}

orxU32 Outpost::GetU32FromConfig(orxSTRING section, orxSTRING property){
	orxConfig_PushSection(section);
	orxU32 value = orxConfig_GetU32(property);
	orxConfig_PopSection();
	
	return value;
}

int Outpost::GetLevelFromConfig() {
	int level = 0;

	orxConfig_PushSection("Game");
	level = orxConfig_GetU32("Level");
	orxConfig_PopSection();

	return level;
}

void Outpost::SetLevelInConfig(int newLevel) {
	orxConfig_PushSection("Game");
	orxConfig_SetU32("Level", newLevel);
	orxConfig_PopSection();

	orxCHAR formattedLevel[50];
	orxString_Print(formattedLevel, "level %d", newLevel);

	orxConfig_PushSection("LevelBannerText");
	orxConfig_SetString("String", formattedLevel);
	orxConfig_PopSection();

	orxObject_SetTextString(levelTextObject, formattedLevel);

}

void Outpost::UpdateGameLevelDetailsInConfig() {
	int level = GetLevelFromConfig();

	int alienCount = level * 5;
	float delayBetween = 2.0 + (((float)(10 - level)) / 4.0);
	if (delayBetween < 0.3) {
		delayBetween = 0.3;
	}
	//orxLOG("delayBetween %f", delayBetween);

	orxConfig_PushSection("AlienSpawner");
	orxConfig_SetU32("ActiveObject", alienCount);
	orxConfig_SetFloat("WaveDelay", delayBetween);
	orxConfig_PopSection();

	orxConfig_PushSection("AsteroidSpawner");
	orxConfig_SetU32("ActiveObject", level * 2);
	orxConfig_PopSection();

	const orxVECTOR alienSpeed = { 0, (orxFLOAT)(-200.0 - (10.0 *  ((orxFLOAT)level)    )), 0 };

	orxConfig_PushSection("Alien");
	orxConfig_SetVector("Speed", &alienSpeed);
	orxConfig_PopSection();

	SetAlienSpawnRatioToConfig();

	//orxLOG("alienSpeed %d", alienSpeed.fY );

}

void Outpost::DeleteCurrentAsteroidCreator() {
	orxOBJECT *asteroidCreator = GetChildObjectFromScene("AsteroidCreator");
	if (asteroidCreator != orxNULL) {
		orxObject_SetLifeTime(asteroidCreator, 0);
	}
}

void Outpost::RecreateAsteroidCreator() {
	DeleteCurrentAsteroidCreator();

	orxOBJECT *asteroidCreator = orxObject_CreateFromConfig("AsteroidCreator");
	orxObject_SetParent(asteroidCreator, scene);
	orxObject_SetOwner(asteroidCreator, scene);
}

void Outpost::DeleteCurrentAlienLauncher() {
	orxOBJECT *alienLauncher = GetChildObjectFromScene("AlienLauncher");
	if (alienLauncher != orxNULL) {
		orxObject_SetLifeTime(alienLauncher, 0);
	}
}

void Outpost::RecreateAlienLauncher() {
	DeleteCurrentAlienLauncher();

	orxOBJECT *alienLauncher = orxObject_CreateFromConfig("AlienLauncher");
	orxObject_SetParent(alienLauncher, scene);
	orxObject_SetOwner(alienLauncher, scene);
}

/*
alien
alien
alien x 8
*/
void Outpost::SetAlienSpawnRatioToConfig() {
	int level = GetLevelFromConfig();

	orxCHAR formattedLevel[20];
	orxString_Print(formattedLevel, "@SpawnLists.Level%d", level);

	orxConfig_PushSection("AlienSpawner");
	orxConfig_SetString("Object", formattedLevel);
	orxConfig_PopSection();
}

void Outpost::ProgressToNextLevel() {
	StopBonusCountdown();
	StopLevelCountdown();
	
	oreCollected = 0;

	HideGameScene();

	ShowGameScene();

	int level = GetLevelFromConfig();
	if (level < 10) {
		level++;
		SetLevelInConfig(level);
		UpdateGameLevelDetailsInConfig();
		CreateLevelBanner();
	}
	
	if (bonus > 1){
		AddToScore(bonus * 10);
	}
	
	ResetBonus();
	StartLevelCountdown();

}

void Outpost::ReportAlienDistance(orxFLOAT distance) {
	//orxLOG("passed dstance %f", distance);
	//if (shortestAlienDistance == 0 || distance < shortestAlienDistance) {
	//	shortestAlienDistance = distance;
	//}

	distanceEngine->ReportDistance(distance);
}

void Outpost::SetZoomBasedOnDistance(const orxCLOCK_INFO &_pstClockInfo) {
	if (distanceEngine->GetShortestDistance() == 0) {
		return;
	}

	const orxFLOAT ZOOM_UNIT = 0.003 * 60;

	orxCAMERA *camera = this->GetMainCamera();

	orxFLOAT newZoomTarget = (1.0 / (distanceEngine->GetShortestDistance() / 1000));
	if (newZoomTarget < 0.5) {
		newZoomTarget = 0.5;
	}
	if (newZoomTarget > 1.0) {
		newZoomTarget = 1.0;
	}

	if (currentZoom < newZoomTarget) {
		currentZoom += ZOOM_UNIT * _pstClockInfo.fDT;
		if (currentZoom > 1.0) {
			currentZoom = 1.0;
		}
	}
	if (currentZoom > newZoomTarget) {
		currentZoom -= ZOOM_UNIT * _pstClockInfo.fDT;
		if (currentZoom < 0.5) {
			currentZoom = 0.5;
		}
	}

	orxCamera_SetZoom(camera, currentZoom);
}

orxSTATUS orxFASTCALL RenderEventHandler(const orxEVENT *_pstEvent) {
	if (_pstEvent->eType == orxEVENT_TYPE_RENDER) {
		if (_pstEvent->eID == orxRENDER_EVENT_STOP) {

			orxRGBA lineColour;
			lineColour.u8R = 255;
			lineColour.u8G = 128;
			lineColour.u8B = 90;
			lineColour.u8A = 255;

			orxVECTOR lineStart = { 0, 0, 0 };
			orxVECTOR lineEnd = { 800, 600, 0 };

			orxDisplay_DrawLine(&lineStart, &lineEnd, lineColour);

		}
	}

	return orxSTATUS_SUCCESS;
}

void InitTextures()
{
	orxU32      u32X, u32Y, u32Size;
	orxFLOAT    fScreenWidth, fScreenHeight, fSpotRadius;
	orxTEXTURE *pstTexture;
	orxBITMAP  *pstBitmap;
	orxU8      *au8Data;

	// Gets screen size
	orxDisplay_GetScreenSize(&fScreenWidth, &fScreenHeight);

	// Creates background texture
	pstBitmap = orxDisplay_CreateBitmap(256, 256);
	pstTexture = orxTexture_Create();
	orxTexture_LinkBitmap(pstTexture, pstBitmap, "GeneratedScrollTexture", orxTRUE);

	// Allocates pixel buffer
	au8Data = (orxU8 *)orxMemory_Allocate(256 * 256 * sizeof(orxRGBA), orxMEMORY_TYPE_VIDEO);

	// For all pixels
	for (u32Y = 0; u32Y < 256; u32Y++)
	{
		for (u32X = 0; u32X < 256; u32X++)
		{
			orxU32  u32Index;
			orxU8   u8Value;

			// Gets pixel value
			u8Value = (orxU8)(u32X ^ u32Y);

			// Gets pixel index
			u32Index = (u32Y * 256 + u32X) * sizeof(orxRGBA);

			// Stores pixel channels
			au8Data[u32Index] =
				au8Data[u32Index + 1] =
				au8Data[u32Index + 2] = u8Value;
			au8Data[u32Index + 3] = 0xFF;
		}
	}

	// Updates bitmap content
	orxDisplay_SetBitmapData(pstBitmap, au8Data, 256 * 256 * sizeof(orxRGBA));

	// Frees pixel buffer
	orxMemory_Free(au8Data);
}

void InitTexturesB(orxTEXTURE *texture)
{
	orxBITMAP  *pstBitmap;

	orxFLOAT x, y;
	orxTexture_GetSize(texture, &x, &y);

	pstBitmap = orxDisplay_CreateBitmap(x, y);
	orxTexture_LinkBitmap(texture, pstBitmap, "GeneratedScrollTexture", orxFALSE);

	orxCOLOR colour;
	colour.vRGB.fR = 255;
	colour.vRGB.fG = 0;
	colour.vRGB.fB = 0;
	colour.fAlpha = 128;

	orxTexture_SetColor(texture, &colour);
}


void Outpost::ResetZoom(){
	orxCAMERA *camera = this->GetMainCamera();
	currentZoom = 1.0;
	orxCamera_SetZoom(camera, currentZoom);
}

void orxFASTCALL JoystickConnectCheck(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	if (titleEnabled){
		if (orxJoystick_IsConnected(1) == orxTRUE){
			if (controllerIcon == orxNULL){
				controllerIcon = orxObject_CreateFromConfig("Controller");
			}
//			if (orxObject_IsEnabled(controllerIcon) == orxFALSE){
//				orxObject_Enable(controllerIcon, orxTRUE);
//			}
		} else {
			if (controllerIcon != orxNULL){
				orxObject_SetLifeTime(controllerIcon, 0);
				controllerIcon = orxNULL;
			}
//			if (orxObject_IsEnabled(controllerIcon) == orxTRUE) {
//				orxObject_Enable(controllerIcon, orxFALSE);
//			}
		}
	}
	
}

void orxFASTCALL levelCountdownCallback(const orxCLOCK_INFO *_rstClockInfo, void *_pstContext){
	if (levelCountdown > 0){
		levelCountdown--;
	} else {
		Outpost::GetInstance().StartBonusCountdown();
	}
}

void orxFASTCALL bonusCountdownCallback(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	Outpost::GetInstance().ReduceBonus();
}

void Outpost::CameraUpdate(const orxCLOCK_INFO &_rstClockInfo){
	if (inputMode == SPLASH_SCREEN){
		return;
	}
	
	if (gameEnabled == orxFALSE) {
		MoveCameraScrollStars();
	}
	
	ApplyShieldPosAndRotToOutpost(extraRotationToAdd); // don't need this param
}

void Outpost::StopLevelCountdown(){
	orxClock_Pause(levelCountdownClock);
	orxClock_Restart(levelCountdownClock);
}

void Outpost::StopBonusCountdown(){
	orxClock_Pause(bonusCountdownClock);
	orxClock_Restart(bonusCountdownClock);
}

void Outpost::StartBonusCountdown(){
	ResetBonus();
	orxClock_Unpause(bonusCountdownClock);
		
	StopLevelCountdown();
}

void Outpost::StartLevelCountdown(){
	levelCountdown = LEVEL_SECONDS;
	orxClock_Unpause(levelCountdownClock);
}

void Outpost::Update(const orxCLOCK_INFO &_pstClockInfo)
{
	//orxSTATUS status = orxSTATUS_SUCCESS;
		/* Should quit? */

	if (inputMode == SPLASH_SCREEN){
		return;
	}

	
	#ifdef __orxDEBUG__
	if (orxInput_IsActive("NextLevel") && orxInput_HasNewStatus("NextLevel") && IsGameEnabled()) {
		//this->ProgressToNextLevel();
		ProgressToNextLevel();
		//Outpost::GetInstance()->ProgressToNextLevel();
		//SetGameOver();
	}
	#endif // __orxDEBUG__	

	if (orxInput_IsActive("Screenshot") && orxInput_HasNewStatus("Screenshot"))
	{
		// Captures it
		orxScreenshot_Capture();
	}
	
	if (inputMode == PLAYING){
		DoInputs(_pstClockInfo);
	} else {
		DoRemapMenuInputs();
	}

	SetZoomBasedOnDistance(_pstClockInfo);
	
	//return status;
}

void Outpost::LoadHighScoreFromConfig(){
	orxConfig_Load("outpost-highscore.ini");
	highScore = GetU32FromConfig("GameInfo", "HighScore");
	
	SetHighScoreText();
}

void Outpost::UpdateKeyInstructionsText(){
	
	orxSTRING upDownLeftRightString = remapControlMenu->GetDirectionKeysForDisplay();
	upDownLeftRightString = orxString_LowerCase(upDownLeftRightString);
	
	orxSTRING shieldUpDownLeftRightString = remapControlMenu->GetShieldKeysForDisplay();
	shieldUpDownLeftRightString = orxString_LowerCase(shieldUpDownLeftRightString);
	
	orxOBJECT *movementKeysObject = FindObjectByName("MovementKeys");
	if (movementKeysObject != orxNULL){
		orxObject_SetTextString(movementKeysObject, upDownLeftRightString);
	}
	
	orxOBJECT *shieldKeysObject = FindObjectByName("ShieldKeys");
	if (shieldKeysObject != orxNULL){
		orxObject_SetTextString(shieldKeysObject, shieldUpDownLeftRightString);
	}
	
}

void Outpost::OutputTestInfoToLog(){
	return; //disable
	
    //Retina test information
    
    orxFLOAT screenWidth = 0;
    orxFLOAT screenHeight = 0;
    
    orxDisplay_GetScreenSize(&screenWidth, &screenHeight);
    
    orxLOG("orxDisplay_GetScreenSize gives: %f x %f", screenWidth, screenHeight);
    
    
    orxFLOAT viewportWidth = 0;
    orxFLOAT viewportHeight = 0;

    orxViewport_GetSize(this->GetMainViewport(), &viewportWidth, &viewportHeight);

    orxLOG("orxViewport_GetSize gives: %f x %f", viewportWidth, viewportHeight);



    orxFLOAT ratio = orxViewport_GetCorrectionRatio(this->GetMainViewport());

    orxLOG("orxViewport_GetCorrectionRatio gives: %f", ratio);

    orxAABOX box;
    orxCamera_GetFrustum(this->GetMainCamera(), &box);
    
    orxLOG("orxCamera_GetFrustum Top Left is %f x %f", box.vTL.fX, box.vTL.fY);
    orxLOG("orxCamera_GetFrustum Bottom Right is %f x %f", box.vBR.fX, box.vBR.fY);
    

}

void Outpost::InitGame(){
	orxViewport_CreateFromConfig("HudViewport");
	orxViewport_CreateFromConfig("SinusViewport");

	orxObject_CreateFromConfig("Background");
	SwitchToTitleMusic();

	orxObject_CreateFromConfig("SineTextObject");

	orxCLOCK *sampleOutpostPositionClock = orxClock_Create(orx2F(2.0f), orxCLOCK_TYPE_USER);
	orxClock_Register(sampleOutpostPositionClock, SampleOutpostPosition, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

	orxEvent_AddHandler(orxEVENT_TYPE_SHADER, &EventHandler);

	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());

	//SetLevelInConfig(1);
	UpdateGameLevelDetailsInConfig();

	CreateGameObjects();
	CreateTitle();
	//controllerIcon = GetObjectByName("Controller");
	
	
	controlsEnabled = orxFALSE;

	camera = this->GetMainCamera();
	currentZoom = orxCamera_GetZoom(camera);

	//cameraHandle = GetObjectByName("CameraHandle"); 
	cameraHandle = orxObject_CreateFromConfig("CameraHandle");
	orxCamera_SetParent(camera, cameraHandle);

	distanceEngine = new DistanceEngine();

	remapControlMenu = CreateObject<ControlMenu>("ControlMenu");
	remapControlMenu->Hide();

	UpdateKeyInstructionsText();

	joystickThreshold = GetU32FromConfig("Input", "DefaultThreshold");

	orxClock_AddGlobalTimer(JoystickConnectCheck, orx2F(0.05f), -1, orxNULL);

	levelCountdownClock = orxClock_Create(orx2F(1.0f), orxCLOCK_TYPE_USER);
	orxClock_Register(levelCountdownClock, levelCountdownCallback, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_LOWER);
	StopLevelCountdown();
	
	bonusCountdownClock = orxClock_Create(orx2F(0.1f), orxCLOCK_TYPE_USER);
	orxClock_Register(bonusCountdownClock, bonusCountdownCallback, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_LOWER);
	StopBonusCountdown();

	LoadHighScoreFromConfig();

	if (orxConfig_PushSection("Display")){
		defaultGameWidth = orxConfig_GetFloat("ScreenWidth");
		defaultGameHeight = orxConfig_GetFloat("ScreenHeight");
		orxConfig_PopSection();		
	} 

	inputMode = PLAYING;
    
    
    //OutputTestInfoToLog();
    


}

orxSTATUS Outpost::Init()
{
	//bDebugLevelBackup = orxDEBUG_IS_LEVEL_ENABLED(orxDEBUG_LEVEL_JOYSTICK);
	//orxDEBUG_ENABLE_LEVEL(orxDEBUG_LEVEL_JOYSTICK, orxFALSE);

	splashScreen = CreateObject<SplashScreen>("SplashScreen");
	
	/* Preload the outpost title graphics */
//	orxOBJECT *preloadedOutputObject = orxObject_CreateFromConfig("OutpostTitle");
//	orxObject_EnableRecursive(preloadedOutputObject, orxFALSE); //not visible
//	orxObject_SetLifeTime(preloadedOutputObject, 0); //destroy it immediately

	orxSTATUS result = orxSTATUS_SUCCESS;

	
	return result;
}






/** Run function, is called every clock cycle
 */
orxSTATUS Outpost::Run()
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	if (quit == orxTRUE)
	{
		eResult = orxSTATUS_FAILURE;
	}
	if (orxInput_HasBeenActivated("ToggleProfiler"))
	{
		/* Toggles profiler rendering */
		orxConfig_PushSection(orxRENDER_KZ_CONFIG_SECTION);
		orxConfig_SetBool(orxRENDER_KZ_CONFIG_SHOW_PROFILER, !orxConfig_GetBool(orxRENDER_KZ_CONFIG_SHOW_PROFILER));
		orxConfig_PopSection();
	}
	if (orxInput_HasBeenActivated("ToggleFullScreenMode"))
	{
		orxBOOL isFullScreen = orxDisplay_IsFullScreen();
		if (isFullScreen == orxFALSE){
			orxDISPLAY_VIDEO_MODE currentMode = {};
			orxDisplay_GetVideoMode(orxU32_UNDEFINED, &currentMode);
			
			orxDISPLAY_VIDEO_MODE newMode = {};
			newMode.bFullScreen = orxTRUE;
			newMode.u32Width = defaultGameWidth;
			newMode.u32Height = defaultGameHeight;
			newMode.u32Depth = currentMode.u32Depth;
			newMode.u32RefreshRate = currentMode.u32RefreshRate;
			
			orxDisplay_SetVideoMode(&newMode);
		} else {
			orxDisplay_SetFullScreen(false);
		}
		
		//orxLOG("ToggleFullScreen");
        //orxLOG("----------------");
        //OutputTestInfoToLog();
	}



	/* Done! */
	return eResult;
}

/** Exit function
 */
void Outpost::Exit()
{
	//orxDEBUG_ENABLE_LEVEL(orxDEBUG_LEVEL_JOYSTICK, bDebugLevelBackup);
}

/** Locate the starting config .ini file
 */
orxSTATUS Outpost::Bootstrap() const
{
	// Add "../data/config" to the list of locations that config files can be loaded from
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../../../data/config", orxFALSE);
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../../data/config", orxFALSE);
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "./data/config", orxFALSE); //for windows release
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../Resources/data/config", orxFALSE); //For Mac release structures

	return orxSTATUS_SUCCESS;
}

/** Main function
 */
int main(int argc, char **argv)
{

	// Executes game
	Outpost::GetInstance().Execute(argc, argv);

	// Done!
	return EXIT_SUCCESS;
}

