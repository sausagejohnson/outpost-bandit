#include "Seeker.h"

void Seeker::OnCreate()
{
	orxVECTOR newSafePosition = orxVECTOR_0;
	orxVECTOR outpostPosition = orxVECTOR_0;

	orxFLOAT randomRotation = orxMath_GetRandomFloat(0, 359);
	newSafePosition.fX = orxMath_GetRandomFloat(150, 1600);
	orxVector_2DRotate(&newSafePosition, &newSafePosition, randomRotation);

	outpostPosition = Outpost::GetInstance().GetCurrentOutpostPosition();

	newSafePosition.fX += outpostPosition.fX; //add outpost X
	newSafePosition.fY += outpostPosition.fY; //add outpost Y

	this->SetPosition(newSafePosition, orxTRUE);

	

	currentLevel = Outpost::GetInstance().GetLevelFromConfig();

	ScrollObject *spawnerSwitch = this->GetOwnedChild();
	if (spawnerSwitch != orxNULL) {
		spawnerSwitch->Enable(orxFALSE);
	}

}

void Seeker::OnDelete()
{
	// Do nothing when deleted
}


void Seeker::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (Outpost::GetInstance().IsGameEnabled() == orxFALSE) {
		return;
	}


	orxOBJECT *seekerObject = this->GetOrxObject();

	//Get seeker position
	orxVECTOR seekerPosition = orxVECTOR_0;

	GetPosition(seekerPosition, orxTRUE);
	
	//Get last known Position Vector of Block
	orxVECTOR lastKnownOutpostPosition = orxVECTOR_0;
	lastKnownOutpostPosition = Outpost::GetInstance().GetLastKnownOutpostPosition();

	//Get real Position Vector of Block, used to popen fire when close
	orxVECTOR realOutpostPosition = orxVECTOR_0;
	realOutpostPosition = Outpost::GetInstance().GetCurrentOutpostPosition();

	//Get real Direction to fire on outpost
	orxVECTOR realDirectionToOutpost = orxVECTOR_0;
	orxVector_Sub(&realDirectionToOutpost, &realOutpostPosition, &seekerPosition);

	//Get Direction to face outpost
	orxVECTOR directionToOutpost = orxVECTOR_0;
	orxVector_Sub(&directionToOutpost, &lastKnownOutpostPosition, &seekerPosition);

	orxVECTOR speedTowardsOutpost = orxVECTOR_0;
	orxVector_Normalize(&speedTowardsOutpost, &realDirectionToOutpost);
	orxVector_Mulf(&speedTowardsOutpost, &speedTowardsOutpost, 2.5);

	orxObject_ApplyForce(seekerObject, &speedTowardsOutpost, orxNULL);


}

orxBOOL Seeker::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (
			orxString_SearchString(colliderName, "Shield") != orxNULL ||
			orxString_SearchString(colliderName, "Top") != orxNULL ||
			orxString_SearchString(colliderName, "Right") != orxNULL ||
			orxString_SearchString(colliderName, "Bottom") != orxNULL ||
			orxString_SearchString(colliderName, "Left") != orxNULL ||
			orxString_SearchString(colliderName, "Asteroid") != orxNULL ||
			orxString_SearchString(colliderName, "Outpost") != orxNULL
		)
		{

		orxVECTOR seekerPosition = orxVECTOR_0;
		this->GetPosition(seekerPosition, orxTRUE);

		orxOBJECT *explosion = orxObject_CreateFromConfig("AlienExplosion");
		orxObject_SetPosition(explosion, &seekerPosition);

		Outpost::GetInstance().AddToScore(50);

		if (orxString_SearchString(colliderName, "Shield") == orxNULL) {
			//_poCollider->SetLifeTime(0);
		}
		SetLifeTime(0);
	}



	return orxTRUE;
}