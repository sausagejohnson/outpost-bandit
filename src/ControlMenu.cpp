#include "ControlMenu.h"




void ControlMenu::StartRemapInput() {
	isRemapping = orxTRUE;
	orxObject_RemoveFX(highlight, "MenuPulseFX");
	orxObject_AddFX(highlight, "MenuSelectedPulseFX");
	//orxLOG("REMAP ON");
}

void ControlMenu::StopRemapInput() {
	isRemapping = orxFALSE;
	orxObject_RemoveFX(highlight, "MenuSelectedPulseFX");
	orxObject_AddFX(highlight, "MenuPulseFX");
	//orxLOG("REMAP OFF");
}

orxBOOL ControlMenu::IsRemapping() {
	return isRemapping;
}

void ControlMenu::UpdateTextObjectsFromMapping() {
	int displayIndex = 0;
	Mapping currentMapping = controlMappings[displayIndex];

	while (orxString_Compare(currentMapping.bindName, orxSTRING_EMPTY) != 0) {

		orxObject_SetTextString(mappingNameObject[displayIndex], orxString_UpperCase(currentMapping.bindName));
		orxObject_SetTextString(keyTextObject[displayIndex], orxString_UpperCase(currentMapping.keyBinding));
		orxObject_SetTextString(joyTextObject[displayIndex], orxString_UpperCase(currentMapping.joystickBinding));

		displayIndex++;
		currentMapping = controlMappings[displayIndex];

	}
}

void ControlMenu::OnCreate()
{
	
	origin = { -350, -232, -0.1 };
	gap = { 50, 26, 0};
	
	orxU32 inputTotal = 0;

	Mapping *currentMapping = controlMappings;
	for(orxS32 i = 0; i < orxConfig_GetListCount("InputListOrder"); i++)
	{
		orxINPUT_TYPE types[8];
		orxENUM IDs[8];
		orxINPUT_MODE modes[8];

		const orxSTRING bindingName = orxConfig_GetListString("InputListOrder", i);
		orxString_NCopy(currentMapping->bindName, bindingName, orxString_GetCharacterCount(bindingName));
		if(orxInput_GetBindingList(bindingName, types, IDs, modes) != orxSTATUS_FAILURE)
		{
			for(orxU32 j = 0; j < 8; j++)
			{
				const orxSTRING inputName = orxInput_GetBindingName(types[j], IDs[j], modes[j]);
						
				switch(types[j])
				{
					case orxINPUT_TYPE_KEYBOARD_KEY:
						orxString_NCopy(currentMapping->keyBinding, inputName, GetStringLengthEOL(inputName));
						break;

					case orxINPUT_TYPE_JOYSTICK_AXIS:
					case orxINPUT_TYPE_JOYSTICK_BUTTON:
						orxString_NCopy(currentMapping->joystickBinding, inputName,
										GetStringLengthEOL(inputName));
						break;
				}
			}
			currentMapping++;
		}
	}

	orxOBJECT *thisObject = this->GetOrxObject();

		int displayIndex = 0;
		currentMapping = &controlMappings[displayIndex];

		while (orxString_Compare(currentMapping->bindName, orxSTRING_EMPTY) != 0) {
			
			mappingRowObject[displayIndex] = orxObject_CreateFromConfig("RemapRow");
			mappingNameObject[displayIndex] = orxObject_CreateFromConfig("RowBindingTextObject");
			keyTextObject[displayIndex] = orxObject_CreateFromConfig("RowKeyTextObject");
			joyTextObject[displayIndex] = orxObject_CreateFromConfig("RowJoyTextObject");

			orxVECTOR position = origin;
			position.fX = origin.fX;
			position.fY = origin.fY + (gap.fY * (orxFLOAT)displayIndex);
			position.fZ = origin.fZ;

			orxObject_SetParent(mappingRowObject[displayIndex], thisObject);
			orxObject_SetOwner(mappingRowObject[displayIndex], thisObject);
			orxObject_SetPosition(mappingRowObject[displayIndex], &position);

			orxObject_SetParent(mappingNameObject[displayIndex], mappingRowObject[displayIndex]);
			orxObject_SetOwner(mappingNameObject[displayIndex], mappingRowObject[displayIndex]);

			orxObject_SetParent(keyTextObject[displayIndex], mappingRowObject[displayIndex]);
			orxObject_SetOwner(keyTextObject[displayIndex], mappingRowObject[displayIndex]);

			orxObject_SetParent(joyTextObject[displayIndex], mappingRowObject[displayIndex]);
			orxObject_SetOwner(joyTextObject[displayIndex], mappingRowObject[displayIndex]);

			displayIndex++;
			currentMapping = &controlMappings[displayIndex];

		}

		UpdateTextObjectsFromMapping();

		menuTotalCount = displayIndex;

		orxVECTOR highlightPosition = origin;

		highlightPosition.fZ = -0.2;
		highlightPosition.fX += 2;
		highlightPosition.fY += 2;

		highlight = orxObject_CreateFromConfig("SelectedMenuHighlight");
		orxObject_SetParent(highlight, thisObject);
		orxObject_SetOwner(highlight, thisObject);

		orxObject_SetPosition(highlight, &highlightPosition);

		DebugAllBindings();
		RemoveOveriddenBindings();
		DebugAllBindings();
		RealignMappingIndexes(); //realign all after table loaded
		DebugAllBindings();
}

void ControlMenu::OnDelete()
{
	// Do nothing when deleted
}

void ControlMenu::UpdateHighlightRow() {
	orxVECTOR rowPosition = orxVECTOR_0;

	orxObject_GetPosition(highlight, &rowPosition);
	rowPosition.fY = origin.fY + (selectedMenuIndex * gap.fY) + 2;

	orxObject_SetPosition(highlight, &rowPosition);
}


void ControlMenu::Up() {
	//orxLOG("Up");
	
	selectedMenuIndex--;

	if (selectedMenuIndex < 0) {
		selectedMenuIndex = menuTotalCount-1;
	}

	UpdateHighlightRow();
}

void ControlMenu::Down() {
	//orxLOG("Down %d", menuTotalCount);
	selectedMenuIndex++;

	if (selectedMenuIndex >= menuTotalCount) {
		selectedMenuIndex = 0;
	}

	UpdateHighlightRow();
}

void ControlMenu::Hide() {
	//this-> ->Enable(orxFALSE, true);
	this->Enable(orxFALSE, true);
}

void ControlMenu::Show() {
	//this-> ->Enable(orxFALSE, true);
	this->Enable(orxTRUE, true);
}

orxU32 ControlMenu::GetStringLengthEOL(const orxSTRING _zString){
	return orxString_GetCharacterCount(_zString) + 1;
}

void ControlMenu::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (Outpost::GetInstance().IsGameEnabled() == orxFALSE) {
		//return;
	}

	if (isRemapping) {
		orxINPUT_TYPE inputType;
		orxENUM buttonAxisOrKeyID;
		orxFLOAT value;
		orxINPUT_MODE mode = orxINPUT_MODE_FULL;

		//Get a button press. This will be the one assigned to 'XButton'
		if (orxInput_GetActiveBinding(&inputType, &buttonAxisOrKeyID, &value)) {
			
			if (inputType == orxINPUT_TYPE_JOYSTICK_AXIS) { //a button
				//orxLOG("Detected axis to remap");
				if (value < 0) {
					mode = orxINPUT_MODE_NEGATIVE;
				}
				if (value > 0) {
					mode = orxINPUT_MODE_POSITIVE;
				}
			}

			if ( IsAnAnalogueBindName(controlMappings[selectedMenuIndex].bindName) == orxTRUE) {
				mode = orxINPUT_MODE_FULL;
			}

			const orxSTRING pressedInputName = orxInput_GetBindingName(inputType, buttonAxisOrKeyID, mode);
			//down: -0.1 up: axis,button1,0.24 left: axis,button0,-0.15 right: axis,button0,0.15
			//orxLOG("Pressed input %s", pressedInputName);
			if (orxString_Compare(pressedInputName, "KEY_DELETE") == 0) {

				//Get all inputs and buttonIDs assigned 
				orxINPUT_TYPE inputTypes[orxINPUT_KU32_BINDING_NUMBER];
				orxENUM bindingButtonIDs[orxINPUT_KU32_BINDING_NUMBER];
				orxINPUT_MODE inputModes[orxINPUT_KU32_BINDING_NUMBER];
				orxInput_GetBindingList(controlMappings[selectedMenuIndex].bindName, inputTypes, bindingButtonIDs, inputModes);

				//unbind all existing joystick buttons bound
				for (orxU32 i = 0; i < orxINPUT_KU32_BINDING_NUMBER; i++) {
					if (inputTypes[i] == orxINPUT_TYPE_JOYSTICK_BUTTON || inputTypes[i] == orxINPUT_TYPE_JOYSTICK_AXIS) {
						orxSTATUS unBindSuccess = orxInput_Unbind(controlMappings[selectedMenuIndex].bindName, i); //remove joystick binding
						orxString_NCopy(controlMappings[selectedMenuIndex].joystickBinding, orxSTRING_EMPTY,
										orxString_GetCharacterCount(orxSTRING_EMPTY));

					}
					if (inputTypes[i] == orxINPUT_TYPE_KEYBOARD_KEY) {
						orxSTATUS unBindSuccess = orxInput_Unbind(controlMappings[selectedMenuIndex].bindName, i); //remove key binding
						orxString_NCopy(controlMappings[selectedMenuIndex].keyBinding, orxSTRING_EMPTY,
										orxString_GetCharacterCount(orxSTRING_EMPTY));
					}
				}
			}
			else { //all other keys / buttons / axis
				//if (inputType != orxINPUT_TYPE_JOYSTICK_AXIS  ) {
					
					if (inputType == orxINPUT_TYPE_JOYSTICK_BUTTON) { //only allow if it's a joystick button press
						//orxLOG("Remapping Type: %d ID: %d Value: %f", inputType, buttonAxisOrKeyID, value); // 4 0 ?
						orxSTRING bindName = controlMappings[selectedMenuIndex].bindName;
						if (!IsAnAnalogueBindName(bindName)) {
							orxString_NCopy(controlMappings[selectedMenuIndex].joystickBinding, pressedInputName,
											GetStringLengthEOL(pressedInputName));
							orxInput_Unbind(controlMappings[selectedMenuIndex].bindName, MAP_INDEX_FOR_JOY);
							//RebindAllMappingInTableByBindingName(controlMappings[selectedMenuIndex].bindName);
							orxInput_Bind(controlMappings[selectedMenuIndex].bindName, inputType, buttonAxisOrKeyID, orxINPUT_MODE_FULL, MAP_INDEX_FOR_JOY);//-1);
						}
					}
					if (inputType == orxINPUT_TYPE_JOYSTICK_AXIS) { //only allow if it's a joystick axis
						//orxLOG("Remapping Type: %d ID: %d Value: %f", inputType, buttonAxisOrKeyID, value); // 4 0 ?
						orxSTRING bindName = controlMappings[selectedMenuIndex].bindName;
						//if (IsUpDownLeftOrRight(bindName) == orxFALSE) {
							orxString_NCopy(controlMappings[selectedMenuIndex].joystickBinding, pressedInputName,
											GetStringLengthEOL(pressedInputName));
							orxSTATUS status = orxInput_Unbind(controlMappings[selectedMenuIndex].bindName, MAP_INDEX_FOR_JOY); //NEED TO UNBIND ALL AND REBIND ALL
							orxInput_Bind(controlMappings[selectedMenuIndex].bindName, inputType, buttonAxisOrKeyID, mode, MAP_INDEX_FOR_JOY);//-1);
						//}
					}
					if (inputType == orxINPUT_TYPE_KEYBOARD_KEY) { //only allow if it's a key press
						//orxLOG("Remapping Type: %d ID: %d Value: %f", inputType, buttonAxisOrKeyID, value); // 4 0 ?
						orxSTRING bindName = controlMappings[selectedMenuIndex].bindName;
						if (!IsAnAnalogueBindName(bindName)) {
							orxInput_Unbind(controlMappings[selectedMenuIndex].bindName, MAP_INDEX_FOR_KEY);
							orxInput_Bind(controlMappings[selectedMenuIndex].bindName, inputType, buttonAxisOrKeyID, orxINPUT_MODE_FULL, MAP_INDEX_FOR_KEY);
							orxString_NCopy(controlMappings[selectedMenuIndex].keyBinding, pressedInputName,
											GetStringLengthEOL(pressedInputName));
						}
					}
				//}
				

			}
			DebugAllBindings();
			UpdateTextObjectsFromMapping();
			SaveMappingToDisk();

			this->StopRemapInput();
		}
	}

}

void ControlMenu::RemoveOveriddenBindings(){
	/*
	 * CHeck the two indexes. If the second one is the same as the first,
	 * wipe out the first.
	 * */
	 
	for (int i=0; i < 50; i++) { //whip though table for binding names
		orxINPUT_TYPE inputType0;
		orxENUM buttonAxisOrKeyID0;
		orxINPUT_MODE mode0 = orxINPUT_MODE_FULL;
		
		orxINPUT_TYPE inputType1;
		orxENUM buttonAxisOrKeyID1;
		orxINPUT_MODE mode1 = orxINPUT_MODE_FULL;
		if (orxString_Compare(controlMappings[i].bindName, orxSTRING_EMPTY) != 0) {
			orxInput_GetBinding(controlMappings[i].bindName, 0, &inputType0, & buttonAxisOrKeyID0, &mode0);
			orxInput_GetBinding(controlMappings[i].bindName, 1, &inputType1, & buttonAxisOrKeyID1, &mode1);
			
			if (inputType0 == inputType1) { //if they are the same wipe out the first
				orxInput_Unbind(controlMappings[i].bindName, 0); 
			}
		} 

	}
}

/**
 * When controls load, the binding may not be key = 0 and joy = 1.
 * If they are swapped, swap them back.
 */
void ControlMenu::RealignMappingIndexes(){
	for (int i=0; i < 50; i++) { //whip though table for binding names

		orxINPUT_TYPE inputType0;
		orxENUM buttonAxisOrKeyID0;
		orxINPUT_MODE mode0 = orxINPUT_MODE_FULL;
		
		orxINPUT_TYPE inputType1;
		orxENUM buttonAxisOrKeyID1;
		orxINPUT_MODE mode1 = orxINPUT_MODE_FULL;
		
		orxInput_GetBinding(controlMappings[i].bindName, MAP_INDEX_FOR_KEY, &inputType0, & buttonAxisOrKeyID0, &mode0);
		orxInput_GetBinding(controlMappings[i].bindName, MAP_INDEX_FOR_JOY, &inputType1, & buttonAxisOrKeyID1, &mode1);

		if (inputType0 == orxINPUT_TYPE_JOYSTICK_BUTTON || inputType0 == orxINPUT_TYPE_NONE) { //only it's a joystick in 0 
			if (inputType1 == orxINPUT_TYPE_KEYBOARD_KEY) { //only allow if it's a joystick axis
				UnbindAllIndexesByName(controlMappings[i].bindName);
				
				orxInput_Bind(controlMappings[i].bindName, inputType1, buttonAxisOrKeyID1, mode1, MAP_INDEX_FOR_KEY);
				orxInput_Bind(controlMappings[i].bindName, inputType0, buttonAxisOrKeyID0, mode0, MAP_INDEX_FOR_JOY);
			}
			
		}
		
		if (inputType0 == orxINPUT_TYPE_JOYSTICK_AXIS) { 
			if (inputType1 == orxINPUT_TYPE_JOYSTICK_AXIS || inputType1 == orxINPUT_TYPE_NONE) { 
				UnbindAllIndexesByName(controlMappings[i].bindName);
				
				orxInput_Bind(controlMappings[i].bindName, inputType0, buttonAxisOrKeyID0, mode0, MAP_INDEX_FOR_JOY);
			}
		}

	}
}

orxBOOL ControlMenu::IsEqualTo(orxSTRING stringA, orxSTRING stringB){
	return orxString_Compare(stringA, stringB) == 0;
}


orxSTRING ControlMenu::GetShieldKeysForDisplay(){
	const int spaceForKeyName = 10;
	const int keyUnderScoreLength = 4;
	
	orxCHAR ShieldUpChar[spaceForKeyName] = {}; //no good for vs2015
	orxCHAR ShieldDownChar[spaceForKeyName] = {};
	orxCHAR ShieldLeftChar[spaceForKeyName] = {};
	orxCHAR ShieldRightChar[spaceForKeyName] = {};
	
	for (int i=0; i < 50; i++) { //whip though table for binding names
		if (orxString_Compare(controlMappings[i].bindName, orxSTRING_EMPTY) != 0) { 
			for (int j=0; j<orxINPUT_KU32_BINDING_NUMBER; j++){
				orxINPUT_TYPE inputType = orxINPUT_TYPE_NONE;
				orxENUM buttonAxisOrKeyID = -1;
				orxINPUT_MODE mode = orxINPUT_MODE_FULL;
			
				orxInput_GetBinding(controlMappings[i].bindName, j, &inputType, &buttonAxisOrKeyID, &mode);

				if (inputType == orxINPUT_TYPE_KEYBOARD_KEY){
					if (IsEqualTo(controlMappings[i].bindName, "ShieldUp")){
						orxString_Print(ShieldUpChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "ShieldDown")){
						orxString_Print(ShieldDownChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "ShieldLeft")){
						orxString_Print(ShieldLeftChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "ShieldRight")){
						orxString_Print(ShieldRightChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
				}
			}
		}
	}
	
	orxString_Print(shieldKeysForDisplay, "%s,%s,%s,%s", ShieldUpChar, ShieldLeftChar, ShieldDownChar, ShieldRightChar);
	return shieldKeysForDisplay;
}

orxSTRING ControlMenu::GetDirectionKeysForDisplay(){
	const int spaceForKeyName = 10;
	const int keyUnderScoreLength = 4;
	
	orxCHAR UpChar[spaceForKeyName] = {}; //no good for vs2015
	orxCHAR DownChar[spaceForKeyName] = {};
	orxCHAR LeftChar[spaceForKeyName] = {};
	orxCHAR RightChar[spaceForKeyName] = {};
	
	for (int i=0; i < 50; i++) { //whip though table for binding names
		if (orxString_Compare(controlMappings[i].bindName, orxSTRING_EMPTY) != 0) { 
			for (int j=0; j<orxINPUT_KU32_BINDING_NUMBER; j++){
				orxINPUT_TYPE inputType = orxINPUT_TYPE_NONE;
				orxENUM buttonAxisOrKeyID = -1;
				orxINPUT_MODE mode = orxINPUT_MODE_FULL;
			
				orxInput_GetBinding(controlMappings[i].bindName, j, &inputType, &buttonAxisOrKeyID, &mode);
				//orxLOG("Index %d | InputType: %d, KeyID: %d", j, inputType, buttonAxisOrKeyID);
				if (inputType == orxINPUT_TYPE_KEYBOARD_KEY){
					if (IsEqualTo(controlMappings[i].bindName, "GoUp")){
						orxString_Print(UpChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "GoDown")){
						orxString_Print(DownChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "GoLeft")){
						orxString_Print(LeftChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
					if (IsEqualTo(controlMappings[i].bindName, "GoRight")){
						orxString_Print(RightChar, "%s", &controlMappings[i].keyBinding[keyUnderScoreLength]);
					}
				}
			}
			
			//orxLOG("============================================");
			//orxLOG(" ");

		}
	}
	
	orxString_Print(directionKeysForDisplay, "%s,%s,%s,%s", UpChar, LeftChar, DownChar, RightChar);
	return directionKeysForDisplay;
}

void ControlMenu::DebugAllBindings(){
	for (int i=0; i < 50; i++) { //whip though table for binding names
		if (orxString_Compare(controlMappings[i].bindName, orxSTRING_EMPTY) != 0) { 
		//if (orxString_Compare(controlMappings[i].bindName, "AnalogLeftRight") == 0 ||
		//	orxString_Compare(controlMappings[i].bindName, "AnalogUpDown") == 0) { 
			//orxLOG("Bindings for %s", controlMappings[i].bindName);
			//orxLOG("--------------------------------------------");

			for (int j=0; j<orxINPUT_KU32_BINDING_NUMBER; j++){
				orxINPUT_TYPE inputType = orxINPUT_TYPE_NONE;
				orxENUM buttonAxisOrKeyID = -1;
				orxINPUT_MODE mode = orxINPUT_MODE_FULL;
			
				orxInput_GetBinding(controlMappings[i].bindName, j, &inputType, &buttonAxisOrKeyID, &mode);
				//orxLOG("Index %d | InputType: %d, KeyID: %d", j, inputType, buttonAxisOrKeyID);
			}
			
			//orxLOG("============================================");
			//orxLOG(" ");

		}
	}
}

void ControlMenu::UnbindAllIndexesByName(orxSTRING bindingName){
	orxInput_Unbind(bindingName, -1); 
}


orxBOOL ControlMenu::IsAnAnalogueBindName(const orxSTRING bindingName) {
	return orxString_Compare(bindingName, "AnalogLeftRight") == 0 || orxString_Compare(bindingName, "AnalogUpDown") == 0;
}

orxBOOL ControlMenu::IsUpDownLeftOrRight(const orxSTRING bindingName) {
	return	orxString_Compare(bindingName, "GoUp") == 0 || 
			orxString_Compare(bindingName, "GoDown") == 0 ||
			orxString_Compare(bindingName, "GoLeft") == 0 ||
			orxString_Compare(bindingName, "GoRight") == 0;
}


void ControlMenu::SaveMappingToDisk() {

	/*if (orxConfig_PushSection("MainInput") == orxSTATUS_SUCCESS) {

		for (int x = 0; x < menuTotalCount; x++) {
			Mapping mapping = controlMappings[x];

			if (orxString_Compare(mapping.keyBinding, orxSTRING_EMPTY) != 0) {
				orxConfig_SetString(mapping.keyBinding, mapping.bindName);
			}

			if (orxString_Compare(mapping.joystickBinding, orxSTRING_EMPTY) != 0) {
				orxConfig_SetString(mapping.joystickBinding, mapping.bindName);
			}

		}
		orxConfig_PopSection();

		orxVECTOR textMargin = {
			{.fX = 10}, 
			{.fY = 10}, 
			{.fZ = 0}
		};

	}*/


	orxInput_Save("controls.ini");
}


orxBOOL ControlMenu::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName,
	const orxSTRING _zColliderPartName,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (
		orxString_SearchString(colliderName, "Shield") != orxNULL ||
		orxString_SearchString(colliderName, "Top") != orxNULL ||
		orxString_SearchString(colliderName, "Right") != orxNULL ||
		orxString_SearchString(colliderName, "Bottom") != orxNULL ||
		orxString_SearchString(colliderName, "Left") != orxNULL ||
		orxString_SearchString(colliderName, "Asteroid") != orxNULL ||
		orxString_SearchString(colliderName, "Outpost") != orxNULL
		)
	{

	}



	return orxTRUE;
}